#include "mtf/Utilities/netUtils.h"

_MTF_BEGIN_NAMESPACE

namespace utils
{

	/****
	 *Implementation of Global Functions
	 * ****/
	MTFNet* create_network(char *conv_model, char *last_conv_layer, int n_layers, int *n_neurons, char *activ_fn)
	{
		//Hard Coded filenames for now, to be changed later + Creation of Conv layers only, to add fully connected layers
		MTFNet *net = new MTFNet("/usr/data/Models/VGG_Models/VGG_CNN_F.caffemodel", "/home/lubicon/mtf-regnet/Utilities/prototxt/solver.prototxt");

		return net;
	}

	void extract_batch(int bs, int bi, std::vector<cv::Mat> t_data, std::vector<cv::Mat> t_labels, std::vector<cv::Mat> &t_data_b, std::vector<cv::Mat> &t_labels_b)
	{
		t_data_b.clear();
		t_labels_b.clear();
		for(int i = 0; i < bs; i++)
		{
			t_data_b.push_back(t_data[bi*bs + i]);
			t_labels_b.push_back(t_labels[bi*bs + i]);
		}
	}

	void testing_input_blobs(shared_ptr<caffe::Net<float> > net_)
	{
		cout << "working" << endl;
		std::vector<shared_ptr< caffe::Blob< float > > > all_blobs = net_->blobs();
		for(int i = 0; i < all_blobs[0]->shape(0); i++)
		{
			int offset = all_blobs[1]->offset(i);
			cout << "Label is " << (int)all_blobs[1]->mutable_cpu_data()[offset] << endl;

			offset = all_blobs[0]->offset(i);
			float* input_data = &all_blobs[0]->mutable_cpu_data()[offset];
			cv::Mat channel(all_blobs[0]->height(), all_blobs[0]->width(), CV_32FC1, input_data);
			cv::imshow("testing ", channel);
			cv::waitKey();
		}

		for(int l = 0; l < all_blobs.size(); l++)
			cout << "Input" << l << " " << all_blobs[l]->shape_string() << endl;

	}

	void train(MTFNet *network, std::vector<cv::Mat> training_data, std::vector<cv::Mat> training_labels)
	{
		char *save_file = "temp.caffemodel";
		cout << "Starting training or finetuning" << endl;

		int nepochs = 1000, disp_freq = 10, save_freq = 10, bs = 64;
		int nbatches = training_data.size() / bs;

		int iter = 0;

		std::vector<cv::Mat> t_data_b;
		std::vector<cv::Mat> t_labels_b;

		for(int i = 0; i < nepochs; i++)
		{
			for(int j = 0; j < nbatches; j++)
			{
				//extracts minibatch
				extract_batch(bs, j, training_data, training_labels, t_data_b, t_labels_b);

				//set Input Blob
				boost::dynamic_pointer_cast<MemoryDataLayer<float>>(network->net_->layers()[0])->AddMatVector(t_data_b, t_labels_b);
				//testing_input_blobs(net_);

				//train the network for one iteration
				network->solver_->Step(1);

				//Display loss
				if(iter%disp_freq == 0)
				{
					vector<caffe::Blob<float> *>out = network->net_->output_blobs();
					float loss = out[0]->asum_data();
					cout << "Train Loss from iteration " << iter << " is " << loss << endl;
				}

				//save the network
				if(iter%save_freq == 0)
				{
					caffe::NetParameter net_param;
					network->net_->ToProto(&net_param);
					caffe::WriteProtoToBinaryFile(net_param, save_file);
					cout << "Saving Params" << endl;
				}
				iter++;
			}
		}

		caffe::NetParameter net_param;
		network->net_->ToProto(&net_param);
		caffe::WriteProtoToBinaryFile(net_param, save_file);
		cout << "Saving Params" << endl;

	}

	/****
	 *Implementation of MTFNet functionalities
	 * ****/
	MTFNet::MTFNet(char* params_file, char* solver_file)
	{
		// Read Solver Parameters
		caffe::SolverParameter solver_param;
		caffe::ReadProtoFromTextFileOrDie(solver_file, &solver_param);

		//Get GPUs and set Mode of Caffe
		vector<int> gpus;
		get_gpus(&gpus);
		if(gpus.size() == 0)
			Caffe::set_mode(Caffe::CPU);
		else
		{
			Caffe::set_mode(Caffe::GPU);
			caffe::Caffe::SetDevice(0);
		}

		//Create Solver Object
		solver_ = boost::shared_ptr< caffe::Solver<float> >(caffe::SolverRegistry<float>::CreateSolver(solver_param));
		net_ = solver_->net();

		//Load from Pretrained Model if exists
		if(strcmp(params_file, ""))
		{
			cout << "Loading pretrained weights from caffemodel" << endl;
			solver_->net()->CopyTrainedLayersFrom(params_file);
		}

		// Set Number of Channels
		caffe::Blob<float>* input_layer = net_->input_blobs()[0];
		num_channels_ = input_layer->channels();
		CHECK(num_channels_ == 3 || num_channels_ == 1)
			<< "Input layer should have 1 or 3 channels.";

		//Set spatial input size
		input_geometry_ = cv::Size(input_layer->width(), input_layer->height());
		cout << "The input Geometry is " << input_layer->width() << " " << input_layer->height() << endl;
	}

	void MTFNet::get_gpus(vector<int>* gpus)
	{
		int count = 0;

#ifndef CPU_ONLY
		CUDA_CHECK(cudaGetDeviceCount(&count));
#else
		NO_GPU;
#endif

		for(int i = 0; i < count; ++i)
			gpus->push_back(i);
	}
}
_MTF_END_NAMESPACE
