#include "mtf/AM/NCC.h"
#include "mtf/Utilities/imgUtils.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

//! value constructor
NCCParams::NCCParams(ImgParams *img_params,
bool _fast_hess) :
ImgParams(img_params){
	fast_hess = _fast_hess;
}
//! default/copy constructor
NCCParams::NCCParams(NCCParams *params) :
ImgParams(params),
fast_hess(NCC_FAST_HESS){
	if(params){
		fast_hess = params->fast_hess;
	}
}

NCC::NCC(ParamType *ncc_params) : 
AppearanceModel(ncc_params), params(ncc_params){
	name = "ncc";
	printf("\n");
	printf("Initializing Normalized Cross Correlation appearance model with...\n");
	printf("fast_hess: %d\n", params.fast_hess);
	printf("grad_eps: %e\n", params.grad_eps);
	printf("hess_eps: %e\n", params.hess_eps);

	pix_norm_mult = 1;
	pix_norm_add = 0;
}

void NCC::initializeSimilarity(){
	if(!is_initialized.similarity){
		init_pix_vals_cntr.resize(n_pix);
		curr_pix_vals_cntr.resize(n_pix);
	}

	init_pix_mean = I0.mean();
	init_pix_vals_cntr = (I0.array() - init_pix_mean);
	c = init_pix_vals_cntr.norm();

	if(!is_initialized.similarity){
		f = 1;
		curr_pix_mean = init_pix_mean;
		curr_pix_vals_cntr = init_pix_vals_cntr;
		b = c;

		is_initialized.similarity = true;
	}
}

void NCC::initializeGrad(){
	if(!is_initialized.grad){
		df_dIt.resize(n_pix);
		df_dI0.resize(n_pix);

		init_grad_ncntr.resize(n_pix);
		curr_grad_ncntr.resize(n_pix);

		init_pix_vals_cntr_c.resize(n_pix);
		curr_pix_vals_cntr_b.resize(n_pix);

		df_dI0.fill(0);
		df_dIt.fill(0);

		init_grad_ncntr.fill(0);
		curr_grad_ncntr.fill(0);

		init_grad_ncntr_mean = curr_grad_ncntr_mean = 0;

	}
	init_pix_vals_cntr_c = init_pix_vals_cntr.array() / c;

	if(!is_initialized.grad){
		curr_pix_vals_cntr_b = init_pix_vals_cntr_c;
		is_initialized.grad = true;
	}
}

void NCC::updateSimilarity(bool prereq_only){
	curr_pix_mean = It.mean();
	curr_pix_vals_cntr = (It.array() - curr_pix_mean);
	a = (init_pix_vals_cntr.array() * curr_pix_vals_cntr.array()).sum();
	b = curr_pix_vals_cntr.norm();	
	bc = b*c;
	b2c = bc*b;
	f = a / bc;
}

void NCC::updateInitGrad(){
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		curr_pix_vals_cntr_b(pix_id) = curr_pix_vals_cntr(pix_id) / b;
		init_grad_ncntr(pix_id) = (curr_pix_vals_cntr_b(pix_id)-f*init_pix_vals_cntr_c(pix_id)) / c;
	}
	init_grad_ncntr_mean = init_grad_ncntr.mean();
	df_dI0 = init_grad_ncntr.array() - init_grad_ncntr_mean;
}
void NCC::updateCurrGrad(){
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		curr_pix_vals_cntr_b(pix_id) = curr_pix_vals_cntr(pix_id) / b;
		curr_grad_ncntr(pix_id) = (init_pix_vals_cntr_c(pix_id) - f*curr_pix_vals_cntr_b(pix_id)) / b;
	}
	curr_grad_ncntr_mean = curr_grad_ncntr.mean();
	df_dIt = curr_grad_ncntr.array() - curr_grad_ncntr_mean;
}

void NCC::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian){
	int ssm_state_size = init_hessian.rows();
	assert(init_hessian.cols() == ssm_state_size);
	MatrixXd init_pix_jacobian_cntr = (init_pix_jacobian.rowwise() - init_pix_jacobian.colwise().mean()).array() / b;
	init_hessian =
		-f*init_pix_jacobian_cntr.transpose()*init_pix_jacobian_cntr
		-
		(init_pix_jacobian_cntr.transpose()*curr_pix_vals_cntr_b)*(init_pix_vals_cntr_c.transpose()*init_pix_jacobian_cntr)
		-
		(init_pix_jacobian_cntr.transpose()*init_pix_vals_cntr_c)*(curr_pix_vals_cntr_b.transpose()*init_pix_jacobian_cntr)
		+
		3 * (init_pix_jacobian_cntr.transpose()*init_pix_vals_cntr_c)*(init_pix_vals_cntr_c.transpose()*init_pix_jacobian_cntr);
}
void NCC::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian){
	int ssm_state_size = curr_hessian.rows();
	assert(curr_hessian.cols() == ssm_state_size);
	//curr_hessian.fill(0);
	//double b2 = b*b;
	//double additive_factor = similarity / b2;
	//for(int j = 0; j < n_pix; j++){
	//	//double hess_mean = 0;
	//	hess = (3 * curr_pix_vals_cntr_b(j)*curr_pix_vals_cntr_b -
	//		curr_pix_vals_cntr_b(j)*init_pix_vals_cntr_c -
	//		init_pix_vals_cntr_c(j)*curr_pix_vals_cntr_b).array() / b2;
	//	hess(j) += additive_factor;
	//	hess.array() -= hess.mean();
	//	curr_hessian += curr_pix_jacobian.transpose() * hess * curr_pix_jacobian.row(j);
	//}
	MatrixXd curr_pix_jacobian_cntr = (curr_pix_jacobian.rowwise() - curr_pix_jacobian.colwise().mean()).array()/b;
	curr_hessian =
		-f*curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr
		-
		(curr_pix_jacobian_cntr.transpose()*curr_pix_vals_cntr_b)*(init_pix_vals_cntr_c.transpose()*curr_pix_jacobian_cntr)
		-
		(curr_pix_jacobian_cntr.transpose()*init_pix_vals_cntr_c)*(curr_pix_vals_cntr_b.transpose()*curr_pix_jacobian_cntr)
		+
		3*(curr_pix_jacobian_cntr.transpose()*curr_pix_vals_cntr_b)*(curr_pix_vals_cntr_b.transpose()*curr_pix_jacobian_cntr);
	//utils::printMatrix(curr_hessian, "curr_hessian");

}
void NCC::cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian){
	assert(self_hessian.cols() == self_hessian.rows());

	MatrixXd curr_pix_jacobian_cntr = (curr_pix_jacobian.rowwise() - curr_pix_jacobian.colwise().mean()).array() / b;
	//printf("curr_pix_jacobian size: %ld, %ld\t", curr_pix_jacobian.rows(), curr_pix_jacobian.cols());
	//printf("curr_pix_jacobian_cntr size: %ld, %ld\t", curr_pix_jacobian_cntr.rows(), curr_pix_jacobian_cntr.cols());
	//printf("curr_pix_vals_cntr_b size: %ld, %ld\n", curr_pix_vals_cntr_b.rows(), curr_pix_vals_cntr_b.cols());
	//utils::printMatrixToFile(curr_pix_jacobian, "curr_pix_jacobian", "log/ncc_log.txt", "%15.9f", "w");
	//utils::printMatrixToFile(curr_pix_jacobian_cntr, "curr_pix_jacobian_cntr", "log/ncc_log.txt", "%15.9f", "a");
	//utils::printMatrixToFile(curr_pix_vals_cntr_b, "curr_pix_vals_cntr_b", "log/ncc_log.txt", "%15.9f", "a");
	if(params.fast_hess){
		self_hessian =
			-curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr
			+
			curr_pix_jacobian_cntr.transpose()*(
			curr_pix_vals_cntr_b.transpose()*curr_pix_vals_cntr_b
			)*curr_pix_jacobian_cntr;
	} else{
		self_hessian =
			-curr_pix_jacobian_cntr.transpose()*curr_pix_jacobian_cntr
			+
			(curr_pix_jacobian_cntr.transpose()*curr_pix_vals_cntr_b)*(curr_pix_vals_cntr_b.transpose()*curr_pix_jacobian_cntr);
	}
}

void NCC::cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
	const MatrixXd &init_pix_hessian){
	int ssm_state_size = init_hessian.rows();
	assert(init_pix_hessian.rows() == ssm_state_size * ssm_state_size);
	cmptInitHessian(init_hessian, init_pix_jacobian);
	for(int j = 0; j < n_pix; j++){
		init_hessian += Map<MatrixXd>((double*)init_pix_hessian.col(j).data(), ssm_state_size, ssm_state_size) * df_dI0(j);;
	}
}
void NCC::cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
	const MatrixXd &curr_pix_hessian){
	int ssm_state_size = curr_hessian.rows();
	assert(curr_pix_hessian.rows() == ssm_state_size * ssm_state_size);
	cmptCurrHessian(curr_hessian, curr_pix_jacobian);
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		curr_hessian += Map<MatrixXd>((double*)curr_pix_hessian.col(pix_id).data(), ssm_state_size, ssm_state_size) * df_dIt(pix_id);;
	}
}

/*Support for FLANN library*/

void NCC::updateDistFeat(double* feat_addr){
	curr_pix_mean = It.mean();
	double curr_pix_std = sqrt((It.array() - curr_pix_mean).matrix().squaredNorm());
	for(size_t pix = 0; pix < n_pix; pix++) {
		*feat_addr = (It(pix) - curr_pix_mean) / curr_pix_std;
		feat_addr++;
	}
}

double NCC::operator()(const double* a, const double* b, 
	size_t size, double worst_dist) const {
	double num = 0;
	size_t n_pix = size - 1;
	const double* last = a + n_pix;
	const double* lastgroup = last - 3;

	/* Process 4 items with each loop for efficiency. */
	while(a < lastgroup) {

		num += a[0] * b[0] + a[1] * b[1] + a[2] * b[2] + a[3] * b[3];
		a += 4;
		b += 4;
	}
	/* Process last 0-3 pixels.  Not needed for standard vector lengths. */
	while(a < last) {

		num += *a * *b;
		a++;
		b++;
	}
	return -num;
}

_MTF_END_NAMESPACE

