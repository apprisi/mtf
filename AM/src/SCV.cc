#include "mtf/AM/SCV.h"
#include "mtf/Utilities/imgUtils.h"
#include "mtf/Utilities/histUtils.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

//! value constructor
SCVParams::SCVParams(ImgParams *img_params,
bool _use_bspl, int _n_bins, double _pre_seed,
bool _partition_of_unity, bool _weighted_mapping,
bool _mapped_gradient, bool _debug_mode) :
ImgParams(img_params){
	use_bspl = _use_bspl;
	n_bins = _n_bins;
	pre_seed = _pre_seed;
	partition_of_unity = _partition_of_unity;
	weighted_mapping = _weighted_mapping;
	mapped_gradient = _mapped_gradient;
	debug_mode = _debug_mode;
	if(n_bins <= 0)
		n_bins = SCV_N_BINS;
}
//! default/copy constructor
SCVParams::SCVParams(SCVParams *params) :
ImgParams(params),
n_bins(SCV_N_BINS),
pre_seed(SCV_PRE_SEED),
partition_of_unity(SCV_POU),
weighted_mapping(SCV_WEIGHTED_MAPPING),
mapped_gradient(SCV_MAPPED_GRADIENT),
debug_mode(SCV_DEBUG_MODE){
	if(params){
		use_bspl = params->use_bspl;
		n_bins = params->n_bins;
		pre_seed = params->pre_seed;
		partition_of_unity = params->partition_of_unity;
		weighted_mapping = params->weighted_mapping;
		mapped_gradient = params->mapped_gradient;
		debug_mode = params->debug_mode;
		if(n_bins <= 0)
			n_bins = SCV_N_BINS;
	}
}

SCV::SCV(ParamType *scv_params) : SSDBase(scv_params),
params(scv_params), init_img(0, 0, 0){
	name = "scv";

	printf("\n");
	printf("Initializing Sum of Conditional Variance appearance model with:\n");
	printf("use_bspl: %d\n", params.use_bspl);
	printf("n_bins: %d\n", params.n_bins);
	printf("pre_seed: %f\n", params.pre_seed);
	printf("partition_of_unity: %d\n", params.partition_of_unity);
	printf("weighted_mapping: %d\n", params.weighted_mapping);
	printf("mapped_gradient: %d\n", params.mapped_gradient);
	printf("debug_mode: %d\n", params.debug_mode);

	// preseeding the joint histogram by 's' is equivalent to 
	// preseeding individual histograms by s * n_bins
	hist_pre_seed = params.n_bins * params.pre_seed;

	double norm_pix_min = 0, norm_pix_max = params.n_bins - 1;

	if(params.use_bspl && params.partition_of_unity){
		assert(params.n_bins > 3);
		norm_pix_min = 1;
		norm_pix_max = params.n_bins - 2;
	}
	printf("norm_pix_min: %f\n", norm_pix_min);
	printf("norm_pix_max: %f\n", norm_pix_max);

	pix_norm_mult = (norm_pix_max - norm_pix_min) / (PIX_MAX - PIX_MIN);
	pix_norm_add = norm_pix_min;

	if((pix_norm_mult != 1.0) || (pix_norm_add != 0.0)){
		printf("Image normalization is enabled\n");
	}

	intensity_map.resize(params.n_bins);
	init_hist.resize(params.n_bins);
	curr_hist.resize(params.n_bins);
	curr_joint_hist.resize(params.n_bins, params.n_bins);

	if(params.use_bspl){
		init_hist_mat.resize(params.n_bins, n_pix);
		curr_hist_mat.resize(params.n_bins, n_pix);
		_init_bspl_ids.resize(n_pix, Eigen::NoChange);
		_curr_bspl_ids.resize(n_pix, Eigen::NoChange);
		_std_bspl_ids.resize(params.n_bins, Eigen::NoChange);
		for(int i = 0; i < params.n_bins; i++) {
			_std_bspl_ids(i, 0) = max(0, i - 1);
			_std_bspl_ids(i, 1) = min(params.n_bins - 1, i + 2);
		}
	}
}

void SCV::initializePixVals(const Matrix2Xd& init_pts){
	if(!is_initialized.pix_vals){
		I0.resize(n_pix);
		It.resize(n_pix);
	}

	utils::getPixVals(I0, curr_img, init_pts, n_pix,
		img_height, img_width, pix_norm_mult, pix_norm_add);

	//if (pix_norm_mult != 1.0){ init_pix_vals /= pix_norm_mult; }
	//if (pix_norm_add != 0.0){ init_pix_vals = init_pix_vals.array() + pix_norm_add; }
	if(params.use_bspl){
		utils::getBSplHist(init_hist, init_hist_mat, _init_bspl_ids,
			I0, _std_bspl_ids, params.pre_seed, n_pix);
	}

	if(params.debug_mode){
		utils::printMatrixToFile(I0, "init_pix_vals", "log/mtf_log.txt", "%15.9f", "w");
	}
	// create a copy of the initial pixel values which will hold the original untampered template 
	// that will be used for remapping the initial pixel values when the intensity map is updated; 
	// it will also be used for updating the intensity map itself since using the remapped
	// initial pixel values to update the joint probability distribution and thus the intensity map will cause bias
	orig_init_pix_vals = I0;

	if(params.mapped_gradient){
		// save a copy of the initial image to recompute the mapped image gradient when the intensity
		// map is updated with a new image
		_init_img = curr_img;
		new (&init_img) EigImgT(_init_img.data(), _init_img.rows(), _init_img.cols());
	}
	if(!is_initialized.pix_vals){
		It = I0;
		is_initialized.pix_vals = true;
	}
}

void SCV::initializePixGrad(const Matrix2Xd &init_pts){

	if(!is_initialized.pix_grad){
		dI0_dx.resize(n_pix, Eigen::NoChange);
		dIt_dx.resize(n_pix, Eigen::NoChange);
	}

	utils::getImgGrad(dI0_dx, curr_img, init_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(params.mapped_gradient){
		// save a copy of the initial points to recompute the mapped gradient of the initial image
		// when the intensity map is updated with a new image
		this->init_pts = init_pts;
	}
	if(!is_initialized.pix_grad){
		setCurrPixGrad(getInitPixGrad());
		is_initialized.pix_grad = true;
	}
}

void SCV::initializePixGrad(const Matrix8Xd &warped_offset_pts){

	if(!is_initialized.pix_grad){
		dI0_dx.resize(n_pix, Eigen::NoChange);
		dIt_dx.resize(n_pix, Eigen::NoChange);
	}
	utils::getWarpedImgGrad(dI0_dx,
		curr_img, warped_offset_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(params.mapped_gradient){
		// save a copy of the initial warped offset pts to recompute the mapped gradient of the initial image
		// when the intensity map is updated with a new image
		init_warped_offset_pts = warped_offset_pts;
	}
	if(!is_initialized.pix_grad){
		setCurrPixGrad(getInitPixGrad());
		is_initialized.pix_grad = true;
	}
}



void SCV::updatePixVals(const Matrix2Xd& curr_pts){
	utils::getPixVals(It, curr_img, curr_pts, n_pix,
		img_height, img_width, pix_norm_mult, pix_norm_add);

	if(params.use_bspl){
		utils::getBSplJointHist(curr_joint_hist, curr_hist, curr_hist_mat, _curr_bspl_ids,
			It, _init_bspl_ids, init_hist_mat, _std_bspl_ids,
			hist_pre_seed, params.pre_seed, n_pix);
	} else{
		utils::getDiracJointHist(curr_joint_hist, curr_hist, init_hist,
			It, orig_init_pix_vals, 0, 0, n_pix, params.n_bins);
	}

	for(int j = 0; j < params.n_bins; j++){
		if(init_hist(j) == 0){
			// since the sum of all entries in a column of the joint histogram is zero
			// each individual entry must be zero too
			intensity_map(j) = j;
		} else{
			double wt_sum = 0;
			for(int i = 0; i < params.n_bins; i++){
				wt_sum += i * curr_joint_hist(i, j);
			}
			intensity_map(j) = wt_sum / init_hist(j);
		}

	}
	if(params.weighted_mapping){
		utils::mapPixVals<utils::InterpType::Linear>(I0, orig_init_pix_vals, intensity_map, n_pix);
	} else{
		utils::mapPixVals<utils::InterpType::Nearest>(I0, orig_init_pix_vals, intensity_map, n_pix);
	}	
}

void SCV::updatePixGrad(const Matrix2Xd &curr_pts){
	utils::getImgGrad(dIt_dx, curr_img, curr_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(params.mapped_gradient){
		if(params.weighted_mapping){
			utils::getImgGrad<utils::InterpType::Linear>(dI0_dx,
				curr_img, intensity_map, init_pts,
				grad_eps, n_pix, img_height, img_width);
		} else{
			utils::getImgGrad<utils::InterpType::Nearest>(dI0_dx,
				curr_img, intensity_map, init_pts,
				grad_eps, n_pix, img_height, img_width);
		}
	}
}

void SCV::updatePixGrad(const Matrix8Xd &warped_offset_pts){
	utils::getWarpedImgGrad(dIt_dx, curr_img, warped_offset_pts,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);
	if(params.mapped_gradient){
		if(params.weighted_mapping){
			utils::getWarpedImgGrad<utils::InterpType::Linear>(dI0_dx,
				curr_img, intensity_map, init_warped_offset_pts,
				grad_eps, n_pix, img_height, img_width, pix_norm_mult);
		} else{
			utils::getWarpedImgGrad<utils::InterpType::Nearest>(dI0_dx,
				curr_img, intensity_map, init_warped_offset_pts,
				grad_eps, n_pix, img_height, img_width, pix_norm_mult);
		}
	}
}

void SCV::updatePixHess(const Matrix2Xd &curr_pts){
	utils::getImgHess(d2It_dx2, curr_img, curr_pts, hess_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(params.mapped_gradient){
		if(params.weighted_mapping){
			utils::getImgHess<utils::InterpType::Linear>(d2I0_dx2,
				curr_img, intensity_map, init_pts,
				hess_eps, n_pix, img_height, img_width);
		} else{
			utils::getImgHess<utils::InterpType::Nearest>(d2I0_dx2,
				curr_img, intensity_map, init_pts,
				hess_eps, n_pix, img_height, img_width);
		}
	}
}

_MTF_END_NAMESPACE

