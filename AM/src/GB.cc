#include "mtf/AM/GB.h"

_MTF_BEGIN_NAMESPACE


void GB::initialize(VectorXd &I, VectorXd &a){
	
}
void GB::apply(VectorXd &g, VectorXd &I, VectorXd &a){
	
}
void GB::compositionalUpdate(VectorXd &new_a, VectorXd &old_a, VectorXd &da){
	
}
void GB::cmptParamGrad(VectorXd &df_da, const VectorXd &df_dg, const VectorXd &a){
	
}
void GB::cmptPixGrad(VectorXd &df_dI, const VectorXd &df_dg, const VectorXd &a){
	
}

_MTF_END_NAMESPACE




