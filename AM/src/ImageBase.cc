#include "mtf/AM/ImageBase.h"
#include "mtf/Utilities/imgUtils.h"

_MTF_BEGIN_NAMESPACE

ImgParams::ImgParams(int _resx, int _resy,
double _grad_eps, double _hess_eps,
IlluminationModel *_ilm) :
resx(_resx), resy(_resy),
grad_eps(_grad_eps), hess_eps(_hess_eps),
ilm(_ilm){}
ImgParams::ImgParams(int _resx, int _resy, const cv::Mat &_cv_img,
	double _grad_eps, double _hess_eps, IlluminationModel *_ilm) :
	resx(_resx), resy(_resy),
	grad_eps(_grad_eps), hess_eps(_hess_eps),
	cv_img(_cv_img), ilm(_ilm){}
ImgParams::ImgParams(ImgParams *img_params) :
resx(RES_X),
resy(RES_Y),
grad_eps(RES_X),
hess_eps(RES_Y),
ilm(nullptr){
	if(img_params){
		resx = img_params->resx;
		resy = img_params->resy;
		grad_eps = img_params->grad_eps;
		hess_eps = img_params->hess_eps;
		cv_img = img_params->cv_img;
		ilm = img_params->ilm;
	}
}

ImageBase::ImageBase(ImgParams *img_params) :
curr_img(nullptr, 0, 0), img_height(0), img_width(0),
resx(0), resy(0), n_pix(0), frame_count(0),
pix_norm_add(0.0), pix_norm_mult(1.0),
grad_eps(GRAD_EPS), hess_eps(HESS_EPS) {
	if(img_params) {
		if(img_params->resx <= 0 || img_params->resy <= 0) {
			throw std::invalid_argument("ImageBase::Invalid sampling resolution provided");
		}
		resx = img_params->resx;
		resy = img_params->resy;
		n_pix = resx*resy;
		// defaults to 1 since the default input type is CV_32FC1
		n_channels = 1;
		grad_eps = img_params->grad_eps;
		hess_eps = img_params->hess_eps;

		if(!img_params->cv_img.empty()){
			img_height = img_params->cv_img.rows;
			img_width = img_params->cv_img.cols;
			new (&curr_img) EigImgT((EigPixT*)((img_params->cv_img).data), img_height, img_width);
		}
	}
}

void ImageBase::initializePixVals(const Matrix2Xd& init_pts){
	if(!isInitialized()->pix_vals){
		I0.resize(n_pix);
		It.resize(n_pix);
#if !defined DEFAULT_PIX_INTERP_TYPE
		printf("Using pixel interpolation type: %s\n", utils::toString(PIX_INTERP_TYPE));
#endif
#if !defined DEFAULT_PIX_BORDER_TYPE
		printf("Using pixel border type: %s\n",utils::toString(PIX_BORDER_TYPE));
#endif
	}
	++frame_count;
	utils::getPixVals(I0, curr_img, init_pts, n_pix,
		img_height, img_width, pix_norm_mult, pix_norm_add);

	if(!isInitialized()->pix_vals){
		It = I0;
		isInitialized()->pix_vals = true;
	}
}

void ImageBase::initializePixGrad(const Matrix3Xd& init_pts_hm,
	const Matrix3d &init_warp){

	if(!isInitialized()->pix_grad){
		dI0_dx.resize(n_pix, Eigen::NoChange);
		dIt_dx.resize(n_pix, Eigen::NoChange);
	}
	utils::getWarpedImgGrad(dI0_dx,
		curr_img, init_pts_hm, init_warp,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}
void ImageBase::initializePixGrad(const Matrix2Xd &init_pts){

	if(!isInitialized()->pix_grad){
		dI0_dx.resize(n_pix, Eigen::NoChange);
		dIt_dx.resize(n_pix, Eigen::NoChange);
	}

	utils::getImgGrad(dI0_dx, curr_img, init_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);


	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}

void ImageBase::initializePixGrad(const Matrix8Xd &warped_offset_pts){

	if(!isInitialized()->pix_grad){
#if !defined DEFAULT_GRAD_INTERP_TYPE
		printf("Using gradient interpolation type: %s\n",
			utils::toString(GRAD_INTERP_TYPE));
#endif
		dI0_dx.resize(n_pix, Eigen::NoChange);
		dIt_dx.resize(n_pix, Eigen::NoChange);
	}
	utils::getWarpedImgGrad(dI0_dx,
		curr_img, warped_offset_pts, grad_eps, n_pix,
		img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_grad){
		setCurrPixGrad(getInitPixGrad());
		isInitialized()->pix_grad = true;
	}
}

void ImageBase::initializePixHess(const Matrix3Xd& init_pts_hm,
	const Matrix3d &init_warp){
	if(!isInitialized()->pix_hess){
		d2I0_dx2.resize(Eigen::NoChange, n_pix);
		d2It_dx2.resize(Eigen::NoChange, n_pix);
	}
	utils::getWarpedImgHess(d2I0_dx2, curr_img, init_pts_hm,
		init_warp, hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}

}

void ImageBase::initializePixHess(const Matrix2Xd& init_pts,
	const Matrix16Xd &warped_offset_pts){
	if(!isInitialized()->pix_hess){
		d2I0_dx2.resize(Eigen::NoChange, n_pix);
		d2It_dx2.resize(Eigen::NoChange, n_pix);
	}
	utils::getWarpedImgHess(d2I0_dx2, curr_img, init_pts, warped_offset_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}
}


void ImageBase::initializePixHess(const Matrix2Xd &init_pts){
	if(!isInitialized()->pix_hess){
#if !defined DEFAULT_HESS_INTERP_TYPE
		printf("Using Hessian interpolation type: %s\n",
			utils::toString(HESS_INTERP_TYPE));
#endif
		d2I0_dx2.resize(Eigen::NoChange, n_pix);
		d2It_dx2.resize(Eigen::NoChange, n_pix);
	}
	utils::getImgHess(d2I0_dx2, curr_img, init_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);

	if(!isInitialized()->pix_hess){
		setCurrPixHess(getInitPixHess());
		isInitialized()->pix_hess = true;
	}
}

void ImageBase::extractPatch(VectorXd &pix_vals, const Matrix2Xd& pts){
	utils::getPixVals(pix_vals, curr_img, pts, n_pix, img_height, img_width);
}
void ImageBase::updatePixVals(const Matrix2Xd& curr_pts){
	utils::getPixVals(It, curr_img, curr_pts, n_pix, img_height, img_width,
		pix_norm_mult, pix_norm_add);
}

void ImageBase::updatePixGrad(const Matrix2Xd &curr_pts){
	utils::getImgGrad(dIt_dx, curr_img, curr_pts,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixHess(const Matrix2Xd &curr_pts){
	utils::getImgHess(d2It_dx2, curr_img, curr_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixGrad(const Matrix8Xd &warped_offset_pts){
	utils::getWarpedImgGrad(dIt_dx, curr_img, warped_offset_pts,
		grad_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updatePixHess(const Matrix2Xd& curr_pts,
	const Matrix16Xd &warped_offset_pts){
	utils::getWarpedImgHess(d2It_dx2, curr_img, curr_pts, warped_offset_pts,
		hess_eps, n_pix, img_height, img_width, pix_norm_mult);
}

void ImageBase::updateTemplate(const Matrix2Xd& curr_pts){
	++frame_count;
	// update the template, aka init_pix_vals with the running average of patch corresponding to the provided points
	for(int pix_id = 0; pix_id < n_pix; pix_id++){
		double pix_val = pix_norm_mult * utils::getPixVal<PIX_INTERP_TYPE, PIX_BORDER_TYPE>(
			curr_img, curr_pts(0, pix_id), curr_pts(1, pix_id),
			img_height, img_width) + pix_norm_add;
		I0(pix_id) += (pix_val - I0(pix_id)) / frame_count;
	}
}

_MTF_END_NAMESPACE