#ifndef MTF_GB_H
#define MTF_GB_H

#include "IlluminationModel.h"

_MTF_BEGIN_NAMESPACE
// Global Gain and Bias illumination model
class GB : public IlluminationModel{
public:

	GB() : IlluminationModel(){ name = "gb"; }
	virtual ~GB(){}
	int getStateSize() override{ return 2; };
	void apply(double *g, const double *I, const double *p) override{
		Map<RowVectorXd>(g, n_pix) = (1+p[0]) * Map<const VectorXd>(I, n_pix, 1).array() + p[1];
	}
	void invertState(double *inv_a, double *p){
		inv_a[0] = -p[0] / (1 + p[0]);
		inv_a[1] = -p[1] / (1 + p[0]);
	}
	void compositionalUpdate(double *new_p, const double *old_p, const double *dp) override{
		new_p[0] = old_p[0] * dp[0];
		new_p[1] = old_p[1] * dp[0] + dp[1];
	}
	void cmptParamGrad(double *df_dp, const double *df_dg,
		const double *I, const double *p) override{
		df_dp[0] = Map<const RowVectorXd>(df_dg, n_pix)*Map<const VectorXd>(I, n_pix);
		df_dp[1] = Map<const RowVectorXd>(df_dg, n_pix).sum();
	}
	void cmptPixGrad(double *df_dI, const double *df_dg,
		const double *I, const double *p) override{
		Map<RowVectorXd>(df_dI, n_pix) = Map<const RowVectorXd>(df_dg, n_pix)*(1+p[0]);
	}
};

_MTF_END_NAMESPACE

#endif



