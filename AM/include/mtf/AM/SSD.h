#ifndef MTF_SSD_H
#define MTF_SSD_H

#include "SSDBase.h"

_MTF_BEGIN_NAMESPACE

class SSD : public SSDBase{
public:

	typedef ImgParams ParamType;

	SSD(ParamType *ssd_params);
	SSD() : SSDBase(){}
};

_MTF_END_NAMESPACE

#endif