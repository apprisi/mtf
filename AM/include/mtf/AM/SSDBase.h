#ifndef MTF_SSD_BASE_H
#define MTF_SSD_BASE_H

#include "AppearanceModel.h"

_MTF_BEGIN_NAMESPACE

//! base class for all appearance models that use the negative L2 norm of the difference between
//! the initial and current pixel values (original or modified) as the appearance model
class SSDBase : public AppearanceModel{
public:
	VectorXdM curr_pix_diff;

	SSDBase(ImgParams *img_params = nullptr);

	int getStateSize() override{ return ilm ? ilm->getStateSize() : 0; }
	bool supportsSPI() override{ return true; }
	void initializeSimilarity() override;
	void initializeGrad() override;
	void initializeHess() override{}

	double getLikelihood() override{
		// since SSD can be numerically too large for exponential function to work,
		// we take the square root of the per pixel SSD instead
		return exp(-sqrt(-f/static_cast<double>(n_pix)));
	}

	//-----------------------------------------------------------------------------------//
	//-------------------------------update functions------------------------------------//
	//-----------------------------------------------------------------------------------//

	//void updateSimilarity(bool prereq_only = true) override;
	//void cmptInitJacobian(RowVectorXd &init_jacobian,
	//	const MatrixXd &init_pix_jacobian) override;
	//void cmptCurrJacobian(RowVectorXd &curr_jacobian,
	//	const MatrixXd &curr_pix_jacobian) override;
	//void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
	//	const MatrixXd &init_pix_jacobian, 
	//	const MatrixXd &curr_pix_jacobian) override;
	//void cmptInitHessian(MatrixXd &init_hessian,
	//	const MatrixXd &init_pix_jacobian) override;
	//void cmptCurrHessian(MatrixXd &curr_hessian,
	//	const MatrixXd &curr_pix_jacobian) override;

	void updateSimilarity(bool prereq_only=true) override{
		if(ilm){
			// It actually refers to g(It, p_am) to keep the illumination model transparent
			// and avoid any unnecessary runtime costs if it is not used
			It_orig = It;
			ilm->apply(It.data(), It_orig.data(), p_am.data());
		}		
		curr_pix_diff = It - I0;
		if(prereq_only){ return; }
#ifndef DISABLE_SPI
		if(spi_mask){
			VectorXd masked_err_vec = VectorXbM((bool*)spi_mask, n_pix).select(curr_pix_diff, 0);
			f = -masked_err_vec.squaredNorm() / 2;
		} else{
#endif
			f = -curr_pix_diff.squaredNorm() / 2;
#ifndef DISABLE_SPI
		}
#endif
	}
	void additiveUpdate(const VectorXd& state_update) override{
		if(ilm){p_am += state_update;}
	}
	void compositionalUpdate(const VectorXd& state_update) override{
		if(ilm){
			ilm->compositionalUpdate(p_am.data(), p_am.data(), state_update.data());
		}
	}


	// nothing is done here since init_grad is same as and shares memory
	// with curr_pix_diff that is updated in updateSimilarity
	// (which in turn is a prerequisite of this)
	void updateInitGrad() override{}
	// curr_grad is same as negative of init_grad
	void updateCurrGrad() override{ 
		df_dIt = -df_dI0;
		if(ilm){
			ilm->cmptPixGrad(df_dIt.data(), df_dIt.data(), It_orig.data(), p_am.data());
		}
	}

	void cmptInitJacobian(RowVectorXd &df_dp,
		const MatrixXd &dI0_dpssm) override{
		if(ilm){
			df_dp.tail(ilm->getStateSize()) = df_dpam;
		}
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(df_dp, spi_mask, df_dI0, dI0_dpssm);
		} else{
#endif
			df_dp.noalias() = df_dI0 * dI0_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptCurrJacobian(RowVectorXd &df_dp,
		const MatrixXd &dIt_dpssm) override{
		if(ilm){
			ilm->cmptParamGrad(df_dp.tail(ilm->getStateSize()).data(), 
				df_dIt.data(), It_orig.data(), p_am.data());			
		}
#ifndef DISABLE_SPI
		if(spi_mask){
			getJacobian(df_dp, spi_mask, df_dIt, dIt_dpssm);
		} else{
#endif
			//printf("df_dp: %ld x %ld\n", df_dp.rows(), df_dp.cols());
			//printf("df_dIt: %ld x %ld\n", df_dIt.rows(), df_dIt.cols());
			//printf("dIt_dpssm: %ld x %ld\n", dIt_dpssm.rows(), dIt_dpssm.cols());
			df_dp.head(dIt_dpssm.cols()).noalias() = df_dIt * dIt_dpssm;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptDifferenceOfJacobians(RowVectorXd &diff_of_jacobians,
		const MatrixXd &dI0_dpssm, const MatrixXd &dIt_dpssm) override{
		if(ilm){

		}
#ifndef DISABLE_SPI
		if(spi_mask){
			getDifferenceOfJacobians(diff_of_jacobians, spi_mask, dI0_dpssm, dIt_dpssm);
		} else{
#endif
			diff_of_jacobians.noalias() = df_dIt * (dI0_dpssm + dIt_dpssm);
#ifndef DISABLE_SPI
		}
#endif
	}

	void cmptInitHessian(MatrixXd &init_hessian,
		const MatrixXd &init_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(init_hessian, spi_mask, init_pix_jacobian);
		} else{
#endif
			init_hessian.noalias() = -init_pix_jacobian.transpose() * init_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}

	void cmptCurrHessian(MatrixXd &curr_hessian,
		const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getHessian(curr_hessian, spi_mask, curr_pix_jacobian);
		} else{
#endif
			curr_hessian.noalias() = -curr_pix_jacobian.transpose() * curr_pix_jacobian;
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptInitHessian(MatrixXd &init_hessian, const MatrixXd &init_pix_jacobian,
		const MatrixXd &init_pix_hessian) override;
	void cmptCurrHessian(MatrixXd &curr_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override;

	void cmptSelfHessian(MatrixXd &self_hessian, 
		const MatrixXd &curr_pix_jacobian) override{
		cmptCurrHessian(self_hessian, curr_pix_jacobian);
	}
	void cmptSelfHessian(MatrixXd &self_hessian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &curr_pix_hessian) override{
		cmptSelfHessian(self_hessian, curr_pix_jacobian);
	}
	//! analogous to cmptDifferenceOfJacobians except for computing the difference between the current and initial Hessians
	void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian,
		const MatrixXd &curr_pix_jacobian) override{
#ifndef DISABLE_SPI
		if(spi_mask){
			getSumOfHessians(sum_of_hessians, spi_mask,
				init_pix_jacobian, curr_pix_jacobian);
		} else{
#endif
			sum_of_hessians.noalias() = -(init_pix_jacobian.transpose() * init_pix_jacobian
				+ curr_pix_jacobian.transpose() * curr_pix_jacobian);
#ifndef DISABLE_SPI
		}
#endif
	}
	void cmptSumOfHessians(MatrixXd &sum_of_hessians,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian,
		const MatrixXd &init_pix_hessian, const MatrixXd &curr_pix_hessian) override;

	/*Support for FLANN library*/
	typedef bool is_kdtree_distance;

	typedef double ElementType;
	typedef double ResultType;
	/**
	* Squared Euclidean distance functor, optimized version
	*/
	/**
	*  Compute the squared Euclidean distance between two vectors.
	*
	*	This is highly optimized, with loop unrolling, as it is one
	*	of the most expensive inner loops.
	*
	*	The computation of squared root at the end is omitted for
	*	efficiency.
	*/
	double operator()(const double* a, const double* b, 
		size_t size, double worst_dist = -1) const override;
	/**
	*	Partial euclidean distance, using just one dimension. This is used by the
	*	kd-tree when computing partial distances while traversing the tree.
	*
	*	Squared root is omitted for efficiency.
	*/
	double accum_dist(const double& a, const double& b, int) const{
		return (a - b)*(a - b);
	}
	void updateDistFeat(double* feat_addr) override{
		for(size_t pix = 0; pix < n_pix; pix++) {
			*feat_addr++ = It(pix);
		}
	}
	void initializeDistFeat() override{}
	void updateDistFeat() override{}
	const double* getDistFeat() override{ return getCurrPixVals().data(); }
	int getDistFeatSize() override{ return n_pix; }
protected:
		PixValT It_orig;
private:	
	// functions to provide support for SPI
	void getJacobian(RowVectorXd &jacobian, const bool *pix_mask,
		const RowVectorXd &curr_grad, const MatrixXd &pix_jacobian);
	void getHessian(MatrixXd &hessian, const bool *pix_mask, const MatrixXd &pix_jacobian);
	void getDifferenceOfJacobians(RowVectorXd &diff_of_jacobians, const bool *pix_mask,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian);
	void getSumOfHessians(MatrixXd &sum_of_hessians, const bool *pix_mask,
		const MatrixXd &init_pix_jacobian, const MatrixXd &curr_pix_jacobian);
};

_MTF_END_NAMESPACE

#endif