#ifndef MTF_ILLUMINATION_MODEL_H
#define MTF_ILLUMINATION_MODEL_H

#include "mtf/Macros/common.h"
#include <stdexcept>

_MTF_BEGIN_NAMESPACE
//! Illumination Model is a parametric function that transforms pixel values extracted 
// from a patch to account for illumination changes
//! g(I, a): R(N) x R(K) -> R(N)

class IlluminationModel{
protected:
	int n_pix;
public:
	string name;
	bool apply_on_init;
	IlluminationModel(){}
	virtual ~IlluminationModel(){}
	virtual int getStateSize() = 0;
	virtual void initialize(double *a, int _n_pix){ n_pix = _n_pix; }
	virtual void apply(double *g, const double *I, const double *a) = 0;
	virtual void compositionalUpdate(double *new_a, const double *old_a, const double *da) = 0;
	virtual	void cmptParamGrad(double *df_da, const double *df_dg,
		const double *I, const double *a) = 0;
	virtual void cmptPixGrad(double *df_dI, const double *df_dg,
		const double *I, const double *a) = 0;
};

_MTF_END_NAMESPACE

#endif



