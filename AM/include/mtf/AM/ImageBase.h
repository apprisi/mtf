#ifndef MTF_IMAGE_BASE_H
#define MTF_IMAGE_BASE_H

#define WARPED_GRAD_EPS 1e-8
#define GRAD_EPS 1e-8
#define HESS_EPS 1

#define PIX_MAX 255.0
#define PIX_MIN 0.0

#define RES_X 50
#define RES_Y 50

#include "IlluminationModel.h"
#include "mtf/Macros/common.h"
#include <stdexcept>

_MTF_BEGIN_NAMESPACE

struct ImgParams{
	// horizontal and vertical sampling resolutions
	int resx, resy;
	//! numerical increment/decrement used for computing image hessian and gradient
	//! using the method of finite differences
	double grad_eps, hess_eps;
	// optional OpenCV image where all images in the tracking sequence will be put
	// this is provided to avoid passing the image again each time it is updated
	cv::Mat cv_img;
	// optional parametric function of pixel values that can account for lighting changes
	IlluminationModel *ilm;
	ImgParams(int _resx, int _resy,
		double _grad_eps = GRAD_EPS,
		double _hess_eps = HESS_EPS,
		IlluminationModel *_ilm = nullptr);
	ImgParams(int _resx, int _resy, 
		const cv::Mat &_cv_img,
		double _grad_eps = GRAD_EPS,
		double _hess_eps = HESS_EPS,
		IlluminationModel *_ilm = nullptr);
	ImgParams(ImgParams *img_params = nullptr);
};

// set of indicator variables to keep track of which dependent state variables need updating and executing
// the update expression for any variable only when at least one of the other variables it depends on has 
// been updated; this can help to increase speed by avoiding repeated computations
struct PixStatus{
	bool pix_vals, pix_grad, pix_hess;

	PixStatus(){ clearPixState(); }

	void setPixState(){
		pix_vals = pix_grad = pix_hess = true;
	}
	void clearPixState(){
		pix_vals = pix_grad = pix_hess = false;
	}
};
class ImageBase{

protected:
	EigImgT curr_img;
	cv::Mat curr_img_cv;

	int img_height, img_width; // height and width of the input images

	int resx, resy; // horizontal and vertical sampling resolutions
	int n_pix; //! no. of pixels in the sampled image patch to be tracked	
	int n_channels;//! no. of values that represent each pixel in the sampled patch

	PixValT I0, It; //! pixel values for all channels in the object patch being tracked (flattened as a vector)
	// let N = n_pix = no. of pixels and C = n_channels = no. of channels

	PixGradT dI0_dx, dIt_dx; //! (N*C) x 2 jacobian of pixel values in the warped image w.r.t. pixel coordinate locations
	//! aka gradient of the warped image or warp of the gradient image depending on the search method

	PixHessT d2I0_dx2, d2It_dx2;//! 4 x (N*C) Hessian of pixel values in the warped image w.r.t. pixel coordinate locations;
	//! each column of this matrix contains the 2x2 hessian for the respective location (for each channel) flasttened in the column major order;

	double pix_norm_add, pix_norm_mult; //! additive and multiplicative factors for normalizing pixel values
	int frame_count;// incremented once during initialization and thereafter everytime the template is updated
	double grad_eps, hess_eps; // offsets to use for computing the numerical image gradient and hessian

public:
	explicit ImageBase(ImgParams *img_params = nullptr);
	virtual ~ImageBase(){}

	// return the type of OpenCV Mat image the AM requires as input; 
	// typically either CV_32FC3 or CV_32FC1
	virtual int inputType() const { return CV_32FC1; }

	virtual void setCurrImg(const cv::Mat &cv_img){
		img_height = cv_img.rows;
		img_width = cv_img.cols;
		new (&curr_img) EigImgT((EigPixT*)(cv_img.data), img_height, img_width);
		curr_img_cv = cv_img;
	}
	virtual const cv::Mat& getCurrImg(){ return curr_img_cv; }
	virtual int getImgHeight(){ return img_height; }
	virtual int getImgWidth(){ return img_width; }

	virtual int getResX(){ return resx; }
	virtual int getResY(){ return resy; }
	virtual int getPixCount(){ return n_pix; }
	virtual int getChannelCount(){ return n_channels; }

	// accessor methods; these are not defined as 'const' since an appearance model may like to
	// make some last moment changes to the variable being accessed before returning it to can avoid any 
	// unnecessary computations concerning the variable (e.g. in its 'update' function) unless it is actually accessed;
	virtual const PixValT& getInitPixVals(){ return I0; }
	virtual const PixGradT& getInitPixGrad(){ return dI0_dx; }
	virtual const PixHessT& getInitPixHess(){ return d2I0_dx2; }

	virtual const PixValT& getCurrPixVals(){ return It; }
	virtual const PixGradT& getCurrPixGrad(){ return dIt_dx; }
	virtual const PixHessT& getCurrPixHess(){ return d2It_dx2; }

	// modifier methods;
	virtual void setInitPixVals(const PixValT &pix_vals){ I0 = pix_vals; }
	virtual void setInitPixGrad(const PixGradT &pix_grad){ dI0_dx = pix_grad; }
	virtual void setInitPixHess(const PixHessT &pix_hess){ d2I0_dx2 = pix_hess; }

	virtual void setCurrPixVals(const PixValT &pix_vals){ It = pix_vals; }
	virtual void setCurrPixGrad(const PixGradT &pix_grad){ dIt_dx = pix_grad; }
	virtual void setCurrPixHess(const PixHessT &pix_hess){ d2It_dx2 = pix_hess; }

	virtual void initializePixVals(const PtsT& init_pts);

	// functions to compute image differentials (gradient and hessian) are overloaded since there are two ways to define them:
	// 1. differential of the warped image: the image is warped first, then its differential is computed at the base (unwarped) locations
	// 2. warp of the image differential:  differential is computed in the image coordinates and evaluated at the given (presumably warped) locations
	//1. gradient of the warped image
	virtual void initializePixGrad(const HomPtsT& init_pts_hm, const ProjWarpT &init_warp);
	virtual void initializePixGrad(const GradPtsT &warped_offset_pts);
	// 2. warp of the image gradient
	virtual void initializePixGrad(const PtsT &init_pts);

	//1. hessian of the warped image
	virtual void initializePixHess(const HomPtsT& init_pts_hm, const ProjWarpT &init_warp);
	virtual void initializePixHess(const PtsT& init_pts, const HessPtsT &warped_offset_pts);
	// 2. warp of the image hessian
	virtual void initializePixHess(const PtsT &init_pts);

	// -------- functions for updating state variables when a new image arrives -------- //
	virtual void updatePixVals(const PtsT& curr_pts);

	virtual void updatePixGrad(const GradPtsT &warped_offset_pts);
	virtual void updatePixGrad(const PtsT &curr_pts);

	virtual void updatePixHess(const PtsT &curr_pts);
	virtual void updatePixHess(const PtsT& curr_pts, const HessPtsT &warped_offset_pts);

	// optional function to incorporate online learning or adaptation of the object template being tracked
	// this should be called with the final location of the object (obtained by the search process) in the curent image
	// by default it updates the template with the running average of all the object patches seen so far
	virtual void updateTemplate(const PtsT& curr_pts);
	// general utility function to extract raw pixel values from the current image at the specified points; 
	// might be useful for visualization purposes as the curr_pix-vals might not have raw pixel values;
	virtual void extractPatch(VectorXd &pix_vals, const PtsT& curr_pts);


	virtual PixStatus* isInitialized() = 0;
	
	virtual double getGradOffset(){ return grad_eps; }
	virtual double getHessOffset(){ return hess_eps; }

	EIGEN_MAKE_ALIGNED_OPERATOR_NEW
};

_MTF_END_NAMESPACE

#endif



