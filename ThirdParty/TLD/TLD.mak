TLD_ROOT_DIR = ThirdParty/TLD
TLD_SRC_DIR = ${TLD_ROOT_DIR}/src
TLD_INCLUDE_DIR = ${TLD_ROOT_DIR}/include
TLD_HEADER_DIR = ${TLD_INCLUDE_DIR}/mtf/${TLD_ROOT_DIR}

THIRD_PARTY_TRACKERS += TLD
_THIRD_PARTY_TRACKERS_SO += opentld cvblobs 
TLD_HEADERS = $(addprefix  ${TLD_HEADER_DIR}/, TLD.h)

TLD_LIB_MODULES = TLDUtil VarianceFilter Clustering DetectionResult DetectorCascade EnsembleClassifier ForegroundDetector MedianFlowTracker NNClassifier mftracker/Median mftracker/BB mftracker/BBPredict mftracker/FBTrack mftracker/Lk  
TLD_LIB_INCLUDES = IntegralImage NormalizedPatch
TLD_LIB_HEADERS = $(addprefix ${TLD_HEADER_DIR}/,$(addsuffix .h, ${TLD_LIB_MODULES} ${TLD_LIB_INCLUDES}))
TLD_LIB_SRC = $(addprefix ${TLD_SRC_DIR}/,$(addsuffix .cpp, ${TLD_LIB_MODULES}))

CVBLOBS_ROOT_DIR = ${TLD_ROOT_DIR}/3rdparty/cvblobs
CVBLOBS_LIB_MODULES = ComponentLabeling blob BlobContour BlobOperators BlobProperties BlobResult
CVBLOBS_LIB_INCLUDES = BlobLibraryConfiguration 
CVBLOBS_LIB_HEADERS = $(addprefix ${CVBLOBS_ROOT_DIR}/,$(addsuffix .h, ${CVBLOBS_LIB_MODULES} ${CVBLOBS_LIB_INCLUDES}))
CVBLOBS_LIB_SRC = $(addprefix ${CVBLOBS_ROOT_DIR}/,$(addsuffix .cpp, ${CVBLOBS_LIB_MODULES}))

THIRD_PARTY_HEADERS += ${TLD_HEADERS} ${TLD_LIB_HEADERS} ${CVBLOBS_LIB_HEADERS} 
THIRD_PARTY_INCLUDE_DIRS += ${TLD_INCLUDE_DIR}

${BUILD_DIR}/TLD.o: ${TLD_SRC_DIR}/TLD.cc ${TLD_HEADERS} ${UTILITIES_HEADER_DIR}/miscUtils.h ${MACROS_HEADER_DIR}/common.h ${ROOT_HEADER_DIR}/TrackerBase.h
	${CXX} -c -fPIC ${WARNING_FLAGS} ${OPT_FLAGS} ${CT_FLAGS} $< ${FLAGS64} ${FLAGSCV} -I${TLD_INCLUDE_DIR} -I${UTILITIES_INCLUDE_DIR} -I${MACROS_INCLUDE_DIR} -I${ROOT_INCLUDE_DIR} -o $@
	
${MTF_LIB_INSTALL_DIR}/libopentld.so: ${TLD_ROOT_DIR}/libopentld.so
	sudo cp -f $< $@
${TLD_ROOT_DIR}/libopentld.so: ${TLD_LIB_SRC} ${TLD_LIB_HEADERS}
	cd ${TLD_ROOT_DIR}; rm -rf Build; mkdir Build; cd Build; cmake ..
	$(MAKE) -C ${TLD_ROOT_DIR}/Build --no-print-directory
	mv ${TLD_ROOT_DIR}/Build/libopentld.so ${TLD_ROOT_DIR}/	
	
${MTF_LIB_INSTALL_DIR}/libcvblobs.so: ${CVBLOBS_ROOT_DIR}/libcvblobs.so
	sudo cp -f $< $@
${CVBLOBS_ROOT_DIR}/libcvblobs.so: ${CVBLOBS_LIB_SRC} ${CVBLOBS_LIB_HEADERS}
	cd ${CVBLOBS_ROOT_DIR}; rm -rf Build; mkdir Build; cd Build; cmake ..
	$(MAKE) -C ${CVBLOBS_ROOT_DIR}/Build --no-print-directory
	mv ${CVBLOBS_ROOT_DIR}/Build/libcvblobs.so ${CVBLOBS_ROOT_DIR}/	