CMT_ROOT_DIR = ThirdParty/CMT
CMT_SRC_DIR = ${CMT_ROOT_DIR}/src
CMT_INCLUDE_DIR = ${CMT_ROOT_DIR}/include
CMT_HEADER_DIR = ${CMT_INCLUDE_DIR}/mtf/${CMT_ROOT_DIR}
CMT_HEADERS = $(addprefix  ${CMT_HEADER_DIR}/, CMT.h)

THIRD_PARTY_TRACKERS += CMT
_THIRD_PARTY_TRACKERS_SO += cmt 

CMT_LIB_MODULES = common Consensus Fusion gui Matcher Tracker getopt/getopt fastcluster/fastcluster
CMT_LIB_INCLUDES = logging/log
CMT_LIB_HEADERS = $(addprefix ${CMT_HEADER_DIR}/,$(addsuffix .h, ${CMT_LIB_MODULES} ${CMT_LIB_INCLUDES}))
CMT_LIB_SRC = $(addprefix ${CMT_SRC_DIR}/,$(addsuffix .cpp, ${CMT_LIB_MODULES}))

THIRD_PARTY_INCLUDE_DIRS += ${CMT_INCLUDE_DIR}
THIRD_PARTY_HEADERS += ${CMT_HEADERS} ${CMT_LIB_HEADERS}

${BUILD_DIR}/CMT.o: ${CMT_SRC_DIR}/CMT.cc ${CMT_HEADERS} ${ROOT_HEADER_DIR}/TrackerBase.h
	${CXX} -fPIC -c ${WARNING_FLAGS} ${OPT_FLAGS} $< -std=c++11 ${FLAGS64} ${FLAGSCV} -I${CMT_INCLUDE_DIR} -I${UTILITIES_INCLUDE_DIR} -I${MACROS_INCLUDE_DIR} -I${ROOT_INCLUDE_DIR} -o  $@

${MTF_LIB_INSTALL_DIR}/libcmt.so: ${CMT_ROOT_DIR}/libcmt.so
	sudo cp -f $< $@	
${CMT_ROOT_DIR}/libcmt.so: ${CMT_LIB_SRC} ${CMT_LIB_HEADERS}
	cd ${CMT_ROOT_DIR}; rm -rf Build; mkdir Build; cd Build; cmake ..
	$(MAKE) -C ${CMT_ROOT_DIR}/Build --no-print-directory
	mv ${CMT_ROOT_DIR}/Build/libcmt.so ${CMT_ROOT_DIR}/