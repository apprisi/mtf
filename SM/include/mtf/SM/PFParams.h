#ifndef MTF_PF_PARAMS_H
#define MTF_PF_PARAMS_H

#include "mtf/Macros/common.h"

#define PF_MAX_ITERS 10
#define PF_N_PARTICLES 200
#define PF_EPSILON 0.01
#define PF_DYN_MODEL 0
#define PF_UPD_TYPE 0
#define PF_LIKELIHOOD_FUNC 0
#define PF_RESAMPLING_TYPE 0
#define PF_RESET_TO_MEAN false
#define PF_MEAN_OF_CORNERS false
#define PF_CORNER_SIGMA_D 0.06
#define PF_PIX_SIGMA 0.04
#define PF_MEASUREMENT_SIGMA 0.1
#define PF_SHOW_PARTICLES 0
#define PF_UPDATE_TEMPLATE 0
#define PF_DEBUG_MODE false

_MTF_BEGIN_NAMESPACE

struct PFParams{
	// supported dynamic models for sample generation
	enum class DynamicModel{
		RandomWalk,
		AutoRegression1
	};
	enum class UpdateType{
		Additive,
		Compositional
	};
	enum class ResamplingType{
		None,
		BinaryMultinomial,
		LinearMultinomial,
		Residual
	};
	enum class LikelihoodFunc{
		AM,
		Gaussian,
		Reciprocal
	};
	static const char* toString(DynamicModel _dyn_model);
	static const char* toString(UpdateType _upd_type);
	static const char* toString(ResamplingType _resampling_type);
	static const char* toString(LikelihoodFunc _likelihood_func);

	int max_iters; //! maximum iterations of the PF algorithm to run for each frame
	int n_particles;
	double epsilon; //! maximum L1 norm of the state update vector at which to stop the iterations	
	DynamicModel dyn_model;
	UpdateType upd_type;
	LikelihoodFunc likelihood_func;
	ResamplingType resampling_type;

	bool reset_to_mean;
	// instead of computing the mean of SSM state samples, get the corners 
	// corresponding to each sample and take the mean of these instead;
	bool mean_of_corners;

	std::vector<double> ssm_sigma;
	double pix_sigma;
	double measurement_sigma;

	int show_particles;
	bool update_template;
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 

	PFParams(int _max_iters, int _n_particles, double _epsilon,
		DynamicModel _dyn_model, UpdateType _upd_type,
		LikelihoodFunc _likelihood_func,
		ResamplingType _resampling_type,
		bool _reset_to_mean, bool _mean_of_corners,
		const vector<double> &_ssm_sigma,
		double _pix_sigma, double _measurement_sigma,
		int _show_particles, bool _update_template,
		bool _debug_mode);
	PFParams(PFParams *params = nullptr);
};

_MTF_END_NAMESPACE

#endif

