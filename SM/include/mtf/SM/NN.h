#ifndef MTF_NN_H
#define MTF_NN_H

#include "SearchMethod.h"
#include "GNN.h"
#include <flann/flann.hpp>
#include "NNParams.h"

_MTF_BEGIN_NAMESPACE

template<class AM, class SSM>
class NN : public SearchMethod < AM, SSM > {
	init_profiling();
	char *log_fname;
	char *time_fname;

	//PixGradT pix_grad_identity;
	//MatrixXd ssm_grad_norm;
	//RowVectorXd ssm_grad_norm_mean;
public:
	typedef flann::Matrix<double> flannMatT;
	typedef flann::Matrix<int> flannResultT;
	typedef flann::Index<AM> flannIdxT;
	typedef gnn::GNN<AM> gnnIdxT;

	//typedef flann::Index<flann::L2<double> > flannIdxT;

	typedef NNParams ParamType;
	ParamType params;
	typedef ParamType::IdxType IdxType;
	typedef ParamType::SearchType SearchType;

	typedef NNIndexParams IdxParamType;
	IdxParamType idx_params;
	flann::SearchParams search_params;

	using SearchMethod<AM, SSM> ::am;
	using SearchMethod<AM, SSM> ::ssm;
	using typename SearchMethod<AM, SSM> ::AMParams;
	using typename SearchMethod<AM, SSM> ::SSMParams;
	using SearchMethod<AM, SSM> ::cv_corners_mat;
	using SearchMethod<AM, SSM> ::name;
	using SearchMethod<AM, SSM> ::initialize;
	using SearchMethod<AM, SSM> ::update;

	int frame_id;
	int am_dist_size;
	int ssm_state_size;

	Matrix3d warp_update;
	VectorXd state_sigma;

	Matrix24d prev_corners;
	VectorXd inv_state_update;
	
	vector<VectorXd> ssm_perturbations;

	MatrixXdr eig_dataset;
	VectorXd eig_query;
	VectorXi eig_result;
	VectorXd eig_dists;

	flannMatT *flann_dataset;
	flannMatT *flann_query;
	flannIdxT *flann_index;
	//flannResultT *flann_result;
	//flannMatT *flann_dists;

	gnnIdxT *gnn_index;

	int best_idx;
	double best_dist;

	string saved_db_path, saved_idx_path;
	cv::Mat curr_img_uchar;
	cv::Point2d curr_corners[4];

	NN(ParamType *nn_params = nullptr, IdxParamType *nn_idx_params = nullptr,
		AMParams *am_params = nullptr, SSMParams *ssm_params = nullptr);
	~NN(){
		delete(flann_dataset);
		delete(flann_query);
		delete(flann_index);
		//delete(flann_result);
		//delete(flann_dists);
	}

	void initialize(const cv::Mat &corners) override;
	void update() override;
	const flann::IndexParams getIndexParams(IdxType index_type);

	//double getCornerChangeNorm(const VectorXd &state_update);
	//double findSSMSigma(int param_id);
	//double findSSMSigmaGrad(int param_id);
};

_MTF_END_NAMESPACE

#endif

