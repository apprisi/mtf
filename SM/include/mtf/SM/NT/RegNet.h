#ifndef MTF_REGNET_NT_H
#define MTF_REGNET_NT_H

#include "SearchMethod.h"
#include "mtf/Utilities/netUtils.h"

#define RegNet_MAX_ITERS 10
#define RegNet_EPSILON 0.01
#define RegNet_CORNER_SIGMA_D 0.06
#define RegNet_CORNER_SIGMA_T 0.04
#define RegNet_PIX_SIGMA 0
#define RegNet_N_SAMPLES 1000
#define RegNet_N_TREES 6
#define RegNet_CONFIG_FILE "Config/nn.cfg"
#define RegNet_ADDITIVE_UPDATE 1
#define RegNet_SHOW_SAMPLES 1
#define RegNet_ADD_POINTS 0
#define RegNet_REMOVE_POINTS 0
#define RegNet_SSM_SIGMA_PREC 1.1
#define RegNet_LOAD_INDEX 0
#define RegNet_SAVE_INDEX 0
#define RegNet_INDEX_FILE_TEMPLATE "nn_saved_index"
#define RegNet_DEBUG_MODE false
#define RegNet_CORNER_GRAD_EPS 1e-5

_MTF_BEGIN_NAMESPACE

struct RegNetParams{
	int max_iters; //! maximum iterations of the RegNet algorithm to run for each frame
	int n_samples;
	double epsilon; //! maximum L2 norm of the state update vector at which to stop the iterations	

	std::vector<double> ssm_sigma;
	double pix_sigma;

	int n_checks;

	bool additive_update;
	int show_samples;
	int add_points;
	int remove_points;

	bool save_index;
	bool load_index;
	std::string saved_index_dir;

	double ssm_sigma_prec; //! precision within which the change in corners produced by the change in
	// a parameter for the chosen sigma should match the desired change in corners
	bool debug_mode; //! decides whether logging data will be printed for debugging purposes; 
	//! only matters if logging is enabled at compile time
	RegNetParams(int _max_iters, int _n_samples, double _epsilon,
		const std::vector<double> &_ssm_sigma,		
		double _pix_sigma, bool _additive_update, int _show_samples,
		int _add_points, int _remove_points,
		bool load_index, bool _save_index, std::string _saved_index_dir,
		double _ssm_sigma_prec, bool _debug_mode);
	RegNetParams(RegNetParams *params = nullptr);
};
namespace nt{


	class RegNet : public SearchMethod{
		init_profiling();
		char *log_fname;
		char *time_fname;

		//PixGradT pix_grad_identity;
		//MatrixXd ssm_grad_norm;
		//RowVectorXd ssm_grad_norm_mean;
	public:
		typedef RegNetParams ParamType;
		ParamType params;

		utils::MTFNet *reg_net;

		using SearchMethod::am;
		using SearchMethod::ssm;
		using SearchMethod::cv_corners_mat;
		using SearchMethod::name;
		using SearchMethod::initialize;
		using SearchMethod::update;

		int frame_id;
		int am_dist_size;
		int ssm_state_size;

		Matrix3d warp_update;
		VectorXd state_sigma;

		Matrix24d prev_corners;
		VectorXd inv_state_update;

		vector<VectorXd> ssm_perturbations;

		MatrixXdr eig_dataset;
		VectorXd eig_query;
		VectorXi eig_result;
		VectorXd eig_dists;

		int best_idx;
		double best_dist;

		string saved_db_path, saved_idx_path;
		cv::Mat curr_img_uchar;
		cv::Point2d curr_corners[4];

		RegNet(AM *_am, SSM *_ssm, ParamType *nn_params = nullptr);
		~RegNet(){}

		void initialize(const cv::Mat &corners) override;
		void update() override;
	};
}

_MTF_END_NAMESPACE

#endif

