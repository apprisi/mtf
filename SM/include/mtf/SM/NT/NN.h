#ifndef MTF_NN_NT_H
#define MTF_NN_NT_H

#include "SearchMethod.h"
#include "mtf/SM/GNN.h"
#include "mtf/SM/NNParams.h"

_MTF_BEGIN_NAMESPACE
namespace nt{
	class NN : public SearchMethod {
		init_profiling();
		char *log_fname;
		char *time_fname;
	public:
		typedef gnn::GNN<AM> gnnIdxT;

		typedef NNParams ParamType;
		ParamType params;
		typedef ParamType::IdxType IdxType;
		typedef ParamType::SearchType SearchType;

		typedef NNIndexParams IdxParamType;
		IdxParamType idx_params;

		int frame_id;
		int am_dist_size;
		int ssm_state_size;

		Matrix3d warp_update;
		VectorXd state_sigma;

		Matrix24d prev_corners;
		VectorXd inv_state_update;

		vector<VectorXd> ssm_perturbations;

		MatrixXdr eig_dataset;

		gnnIdxT *gnn_index;

		int best_idx;
		double best_dist;

		string saved_db_path, saved_idx_path;
		cv::Mat curr_img_uchar;
		cv::Point2d curr_corners[4];

		NN(AM *_am, SSM *_ssm, ParamType *nn_params = nullptr,
			IdxParamType *nn_idx_params = nullptr);
		~NN(){}

		void initialize(const cv::Mat &corners) override;
		void update() override;	
	};
}
_MTF_END_NAMESPACE

#endif

