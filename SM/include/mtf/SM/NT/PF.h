#ifndef MTF_PF_NT_H
#define MTF_PF_NT_H

#include "SearchMethod.h"
#include <boost/random/linear_congruential.hpp>
#include <boost/random/normal_distribution.hpp>
#include <boost/random/uniform_real_distribution.hpp>
#include "mtf/SM/PFParams.h"

_MTF_BEGIN_NAMESPACE
namespace nt{	

	struct Particle{
		VectorXd state[2];
		VectorXd ar[2];
		double wt;
		double cum_wt;
		int curr_set_id;
		void resize(int state_size){
			state[0].resize(state_size);
			state[1].resize(state_size);
			ar[0].resize(state_size);
			ar[1].resize(state_size);
		}
	};

	// Particle Filter
	class PF : public SearchMethod{
	private:
		char *log_fname;
		char *time_fname;

	public:

		// similarity of the initial patch (or template) with itself
		double max_similarity;

		typedef PFParams ParamType;
		ParamType params;

		typedef ParamType::DynamicModel DynamicModel;
		typedef ParamType::UpdateType UpdateType;
		typedef ParamType::LikelihoodFunc LikelihoodFunc;
		typedef ParamType::ResamplingType ResamplingType;

		typedef boost::minstd_rand RandGenT;
		typedef boost::normal_distribution<double> MeasureDistT;
		//typedef boost::variate_generator<RandGenT&, MeasureDistT> MeasureGenT;
		typedef boost::random::uniform_real_distribution<double> ResampleDistT;
		//typedef boost::variate_generator<RandGenT&, ResampleDistT> ResampleGenT;

		using SearchMethod ::am;
		using SearchMethod ::ssm;
		using SearchMethod ::cv_corners_mat;
		using SearchMethod ::name;
		using SearchMethod ::initialize;
		using SearchMethod ::update;

		RandGenT measurement_gen;
		MeasureDistT measurement_dist;

		RandGenT resample_gen;
		ResampleDistT resample_dist;
		typedef ResampleDistT::param_type ResampleDistParamT;

		int frame_id;

		Matrix3d warp_update;

		CornersT mean_corners;
		// corners corresponding to all the SSM states
		std::vector<CornersT> particle_corners;
		CornersT prev_corners;

		VectorXd mean_state;
		// SSM states for all particles
		// 2 sets of particles are stored for efficient resampling
		std::vector<VectorXd> particle_states[2];
		// Update history for Auto Regression
		std::vector<VectorXd> particle_ar[2];
		int curr_set_id;
		VectorXd particle_wts;
		VectorXd particle_cum_wts;

		VectorXd perturbed_state;
		VectorXd perturbed_ar;

		VectorXd state_sigma;
		VectorXi resample_ids;
		VectorXd uniform_rand_nums;

		double measurement_likelihood;
		double measurement_factor;
		cv::Mat curr_img_uchar;

		PF(AppearanceModel *_am, StateSpaceModel *_ssm, 
			ParamType *pf_params = nullptr);
		~PF(){}

		void initialize(const cv::Mat &corners) override;
		void update() override;

		void linearMultinomialResampling();
		void binaryMultinomialResampling();
		void residualResampling();
		void initializeParticles();
		void updateMeanCorners(const VectorXd &particle_state, int particle_id);
		void setRegion(const cv::Mat& corners) override;
	};
}

_MTF_END_NAMESPACE

#endif

