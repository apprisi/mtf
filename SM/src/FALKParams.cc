#include "mtf/SM/FALKParams.h"

_MTF_BEGIN_NAMESPACE

FALKParams::FALKParams(int _max_iters, double _epsilon,
HessType _hess_type, bool _sec_ord_hess,
bool _show_grid, bool _show_patch,
double _patch_resize_factor,
bool _write_frames, bool _upd_templ,
bool _debug_mode) :
max_iters(_max_iters),
epsilon(_epsilon),
hess_type(_hess_type),
sec_ord_hess(_sec_ord_hess),
show_grid(_show_grid),
show_patch(_show_patch),
patch_resize_factor(_patch_resize_factor),
write_frames(_write_frames),
upd_templ(_upd_templ),
debug_mode(_debug_mode){}
FALKParams::FALKParams(FALKParams *params) :
max_iters(FALK_MAX_ITERS),
epsilon(FALK_EPSILON),
hess_type(static_cast<HessType>(FALK_HESS_TYPE)),
sec_ord_hess(FALK_SECOND_ORDER_HESS),
show_grid(FALK_SHOW_GRID),
show_patch(FALK_SHOW_PATCH),
patch_resize_factor(FALK_PATCH_RESIZE_FACTOR),
write_frames(FALK_WRITE_FRAMES),
upd_templ(FALK_UPD_TEMPL),
debug_mode(FALK_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		hess_type = params->hess_type;
		sec_ord_hess = params->sec_ord_hess;
		show_grid = params->show_grid;
		show_patch = params->show_patch;
		patch_resize_factor = params->patch_resize_factor;
		write_frames = params->write_frames;
		upd_templ = params->upd_templ;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE

