#include "mtf/SM/NNParams.h"
#include "mtf/Utilities/miscUtils.h"
#include "mtf/SM/GNN.h"

_MTF_BEGIN_NAMESPACE

const flann::IndexParams NNIndexParams::getIndexParams(IdxType index_type, 
std::string saved_idx_path, bool load_index){
	if(load_index){
		printf("Loading FLANN index from: %s\n", saved_idx_path.c_str());
		return flann::SavedIndexParams(saved_idx_path);
	}
	switch(index_type){
	case IdxType::GNN:
		throw std::invalid_argument("GNN is not a valid FLANN index type");
	case IdxType::Linear:
		printf("Using Linear index\n");
		return flann::LinearIndexParams();
	case IdxType::KDTree:
		printf("Using KD Tree index with:\n");
		printf("n_trees: %d\n", kdt_trees);
		return flann::KDTreeIndexParams(kdt_trees);
	case IdxType::KMeans:
		if(!(km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n",
				km_centers_init);
			km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using KMeans index with:\n");
		printf("branching: %d\n", km_branching);
		printf("iterations: %d\n", km_iterations);
		printf("centers_init: %d\n", km_centers_init);
		printf("cb_index: %f\n", km_cb_index);
		return flann::KMeansIndexParams(km_branching, km_iterations,
			km_centers_init, km_cb_index);
	case IdxType::Composite:
		if(!(km_centers_init == flann::FLANN_CENTERS_RANDOM ||
			km_centers_init == flann::FLANN_CENTERS_GONZALES ||
			km_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n", km_centers_init);
			km_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Composite index with:\n");
		printf("n_trees: %d\n", kdt_trees);
		printf("branching: %d\n", km_branching);
		printf("iterations: %d\n", km_iterations);
		printf("centers_init: %d\n", km_centers_init);
		printf("cb_index: %f\n", km_cb_index);
		return flann::CompositeIndexParams(kdt_trees, km_branching, km_iterations,
			km_centers_init, km_cb_index);
	case IdxType::HierarchicalClustering:
		if(!(hc_centers_init == flann::FLANN_CENTERS_RANDOM ||
			hc_centers_init == flann::FLANN_CENTERS_GONZALES ||
			hc_centers_init == flann::FLANN_CENTERS_KMEANSPP)){
			printf("Invalid method provided for selecting initial centers: %d. Using random centers...\n",
				km_centers_init);
			hc_centers_init = flann::FLANN_CENTERS_RANDOM;
		}
		printf("Using Hierarchical Clustering index with:\n");
		printf("branching: %d\n", hc_branching);
		printf("centers_init: %d\n", hc_centers_init);
		printf("trees: %d\n", hc_trees);
		printf("leaf_max_size: %d\n", hc_leaf_max_size);
		return flann::HierarchicalClusteringIndexParams(hc_branching,
			hc_centers_init, hc_trees, hc_leaf_max_size);
	case IdxType::KDTreeSingle:
		printf("Using KDTreeSingle index with:\n");
		printf("leaf_max_size: %d\n", kdtc_leaf_max_size);
		return flann::KDTreeSingleIndexParams(kdts_leaf_max_size);
		//case KDTreeCuda3d:
		//	printf("Using KDTreeCuda3d index with:\n");
		//	printf("leaf_max_size: %d\n", kdtc_leaf_max_size);
		//	return flann::KDTreeCuda3dIndexParams(kdtc_leaf_max_size);
	case IdxType::Autotuned:
		printf("Using Autotuned index with:\n");
		printf("target_precision: %f\n", auto_target_precision);
		printf("build_weight: %f\n", auto_build_weight);
		printf("memory_weight: %f\n", auto_memory_weight);
		printf("sample_fraction: %f\n", auto_sample_fraction);
		return flann::AutotunedIndexParams(auto_target_precision, auto_build_weight,
			auto_memory_weight, auto_sample_fraction);
	default: printf("Invalid index type specified: %d. Using KD Tree index by default...\n",
		index_type);
		return flann::KDTreeIndexParams(kdt_trees);
	}
}

const char* NNParams::toString(IdxType index_type){
	switch(index_type){
	case IdxType::GNN:
		return "GNN";
	case IdxType::KDTree:
		return "KDTree";
	case IdxType::HierarchicalClustering:
		return "HierarchicalClustering";
	case IdxType::KMeans:
		return "KMeans";
	case IdxType::Composite:
		return "Composite";
	case IdxType::Linear:
		return "Linear";
	case IdxType::KDTreeSingle:
		return "KDTreeSingle";
	case IdxType::KDTreeCuda3d:
		return "KDTreeCuda3d";
	case IdxType::Autotuned:
		return "Autotuned";
	default:
		throw std::invalid_argument("Invalid index type provided");
	}
}

const char* NNParams::toString(SearchType search_type){
	switch(search_type){
	case SearchType::KNN:
		return "KNN";
	case SearchType::Radius:
		return "Radius";
	default:
		throw std::invalid_argument("Invalid search type provided");
	}
}
NNParams::NNParams(
	int _max_iters, int _n_samples, double _epsilon,
	const vector<double> &_ssm_sigma,
	double _corner_sigma_d, double _corner_sigma_t,
	double _pix_sigma, SearchType _search_type,
	IdxType _index_type, int _n_checks,
	bool _additive_update, int _show_samples,
	int _add_points, int _remove_points,
	bool _load_index, bool _save_index, string _saved_index_dir,
	double _ssm_sigma_prec, bool _debug_mode) :
	max_iters(_max_iters),
	n_samples(_n_samples),
	epsilon(_epsilon),
	ssm_sigma(_ssm_sigma),
	corner_sigma_d(_corner_sigma_d),
	corner_sigma_t(_corner_sigma_t),
	pix_sigma(_pix_sigma),
	search_type(_search_type),
	index_type(_index_type),
	n_checks(_n_checks),
	additive_update(_additive_update),
	show_samples(_show_samples),
	add_points(_add_points),
	remove_points(_remove_points),
	load_index(_load_index),
	save_index(_save_index),
	saved_index_dir(_saved_index_dir),
	ssm_sigma_prec(_ssm_sigma_prec),
	debug_mode(_debug_mode){}

NNParams::NNParams(NNParams *params) :
max_iters(NN_MAX_ITERS),
n_samples(NN_N_SAMPLES),
epsilon(NN_EPSILON),
corner_sigma_d(NN_CORNER_SIGMA_D),
corner_sigma_t(NN_CORNER_SIGMA_T),
pix_sigma(NN_PIX_SIGMA),
search_type(static_cast<SearchType>(NN_SEARCH_TYPE)),
index_type(static_cast<IdxType>(NN_INDEX_TYPE)),
n_checks(NN_N_CHECKS),
additive_update(NN_ADDITIVE_UPDATE),
show_samples(NN_SHOW_SAMPLES),
add_points(NN_ADD_POINTS),
remove_points(NN_REMOVE_POINTS),
load_index(NN_LOAD_INDEX),
save_index(NN_SAVE_INDEX),
saved_index_dir(NN_INDEX_FILE_TEMPLATE),
ssm_sigma_prec(NN_SSM_SIGMA_PREC),
debug_mode(NN_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		n_samples = params->n_samples;
		epsilon = params->epsilon;

		ssm_sigma = params->ssm_sigma;
		corner_sigma_d = params->corner_sigma_d;
		corner_sigma_t = params->corner_sigma_t;
		pix_sigma = params->pix_sigma;
		search_type = params->search_type;

		index_type = params->index_type;
		n_checks = params->n_checks;
		additive_update = params->additive_update;
		show_samples = params->show_samples;

		add_points = params->add_points;
		remove_points = params->remove_points;

		load_index = params->load_index;
		save_index = params->save_index;
		saved_index_dir = params->saved_index_dir;
		ssm_sigma_prec = params->ssm_sigma_prec;
		debug_mode = params->debug_mode;
	}
}

NNIndexParams::NNIndexParams(
	int _gnn_degree,
	int _gnn_max_steps,
	int _gnn_cmpt_dist_thresh,
	int _gnn_build_using_flann,
	bool _gnn_verbose,
	int _kdt_trees,
	int _km_branching,
	int _km_iterations,
	flann::flann_centers_init_t _km_centers_init,
	float _km_cb_index,
	int _kdts_leaf_max_size,
	int _kdtc_leaf_max_size,
	int _hc_branching,
	flann::flann_centers_init_t _hc_centers_init,
	int _hc_trees,
	int _hc_leaf_max_size,
	float _auto_target_precision,
	float _auto_build_weight,
	float _auto_memory_weight,
	float _auto_sample_fraction) :
	gnn_degree(_gnn_degree),
	gnn_max_steps(_gnn_max_steps),
	gnn_cmpt_dist_thresh(_gnn_cmpt_dist_thresh),
	gnn_build_using_flann(_gnn_build_using_flann),
	gnn_verbose(_gnn_verbose),
	kdt_trees(_kdt_trees),
	km_branching(_km_branching),
	km_iterations(_km_iterations),
	km_centers_init(_km_centers_init),
	km_cb_index(_km_cb_index),
	kdts_leaf_max_size(_kdts_leaf_max_size),
	kdtc_leaf_max_size(_kdtc_leaf_max_size),
	hc_branching(_hc_branching),
	hc_centers_init(_hc_centers_init),
	hc_trees(_hc_trees),
	hc_leaf_max_size(_hc_leaf_max_size),
	auto_target_precision(_auto_target_precision),
	auto_build_weight(_auto_build_weight),
	auto_memory_weight(_auto_memory_weight),
	auto_sample_fraction(_auto_sample_fraction){}

NNIndexParams::NNIndexParams(NNIndexParams *params) :
gnn_degree(GNN_DEGREE),
gnn_max_steps(GNN_MAX_STEPS),
gnn_cmpt_dist_thresh(GNN_CMPT_DIST_THRESH),
gnn_build_using_flann(GNN_BUILD_USING_FLANN),
gnn_verbose(GNN_VERBOSE),
kdt_trees(NN_KDT_TREES),
km_branching(NN_KM_BRANCHING),
km_iterations(NN_KM_ITERATIONS),
km_centers_init(NN_KM_CENTERS_INIT),
km_cb_index(NN_KM_CB_INDEX),
kdts_leaf_max_size(NN_KDTS_LEAF_MAX_SIZE),
kdtc_leaf_max_size(NN_KDTC_LEAF_MAX_SIZE),
hc_branching(NN_HC_BRANCHING),
hc_centers_init(NN_HC_CENTERS_INIT),
hc_trees(NN_HC_TREES),
hc_leaf_max_size(NN_HC_LEAF_MAX_SIZE),
auto_target_precision(NN_AUTO_TARGET_PRECISION),
auto_build_weight(NN_AUTO_BUILD_WEIGHT),
auto_memory_weight(NN_AUTO_MEMORY_WEIGHT),
auto_sample_fraction(NN_AUTO_SAMPLE_FRACTION){
	if(params){
		gnn_degree = params->gnn_degree;
		gnn_max_steps = params->gnn_max_steps;
		gnn_cmpt_dist_thresh = params->gnn_cmpt_dist_thresh;
		gnn_build_using_flann = params->gnn_build_using_flann;
		gnn_verbose = params->gnn_verbose;
		kdt_trees = params->kdt_trees;
		km_branching = params->km_branching;
		km_iterations = params->km_iterations;
		km_centers_init = params->km_centers_init;
		km_cb_index = params->km_cb_index;
		kdts_leaf_max_size = params->kdts_leaf_max_size;
		kdtc_leaf_max_size = params->kdtc_leaf_max_size;
		hc_branching = params->hc_branching;
		hc_centers_init = params->hc_centers_init;
		hc_trees = params->hc_trees;
		hc_leaf_max_size = params->hc_leaf_max_size;
		auto_target_precision = params->auto_target_precision;
		auto_build_weight = params->auto_build_weight;
		auto_memory_weight = params->auto_memory_weight;
		auto_sample_fraction = params->auto_sample_fraction;
	}
}

_MTF_END_NAMESPACE
