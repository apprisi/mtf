#include "mtf/SM/IALKParams.h"

_MTF_BEGIN_NAMESPACE

IALKParams::IALKParams(int _max_iters, double _epsilon,
HessType _hess_type, bool _sec_ord_hess,
bool _debug_mode
){
	max_iters = _max_iters;
	epsilon = _epsilon;
	hess_type = _hess_type;
	sec_ord_hess = _sec_ord_hess;
	debug_mode = _debug_mode;
}

IALKParams::IALKParams(IALKParams *params) :
max_iters(IALK_MAX_ITERS),
epsilon(IALK_EPSILON),
hess_type(static_cast<HessType>(IALK_HESS_TYPE)),
sec_ord_hess(IALK_SEC_ORD_HESS),
debug_mode(IALK_DEBUG_MODE){
	if(params){
		max_iters = params->max_iters;
		epsilon = params->epsilon;
		hess_type = params->hess_type;
		sec_ord_hess = params->sec_ord_hess;
		debug_mode = params->debug_mode;
	}
}

_MTF_END_NAMESPACE

