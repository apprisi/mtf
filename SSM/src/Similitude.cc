#include "mtf/SSM/Similitude.h"
#include "mtf/Utilities/warpUtils.h"
#include "mtf/Utilities/miscUtils.h"

_MTF_BEGIN_NAMESPACE

Similitude::Similitude(int resx, int resy, SimilitudeParams *params_in) :
ProjectiveBase(resx, resy),
params(params_in){

	printf("\n");
	printf("initializing Similitude state space model with:\n");
	printf("resx: %d\n", resx);
	printf("resy: %d\n", resy);
	printf("c: %f\n", params.c);
	printf("debug_mode: %d\n", params.debug_mode);

	name = "similitude";
	state_size = 4;
	curr_state.resize(state_size);
}
void Similitude::setState(const VectorXd &ssm_state){
	VALIDATE_SSM_STATE(ssm_state);
	curr_state = ssm_state;
	getWarpFromState(curr_warp, curr_state);
	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Similitude::compositionalUpdate(const VectorXd& state_update){
	VALIDATE_SSM_STATE(state_update);

	getWarpFromState(warp_update_mat, state_update);
	curr_warp = curr_warp * warp_update_mat;

	getStateFromWarp(curr_state, curr_warp);

	curr_pts.noalias() = curr_warp.topRows<2>() * init_pts_hm;
	curr_corners.noalias() = curr_warp.topRows<2>() * init_corners_hm;
}

void Similitude :: getWarpFromState(Matrix3d &warp_mat, 
	const VectorXd& ssm_state){
	VALIDATE_SSM_STATE(ssm_state);

	double tx = ssm_state(0);
	double ty = ssm_state(1);
	double a = ssm_state(2);
	double b = ssm_state(3);

	warp_mat(0,0) = 1 + a;
	warp_mat(0,1) = -b;
	warp_mat(0,2) = tx;
	warp_mat(1,0) = b;
	warp_mat(1, 1) = 1 + a;
	warp_mat(1,2) = ty;
	warp_mat(2,0) = 0;
	warp_mat(2,1) = 0;
	warp_mat(2,2) = 1;
}


void Similitude :: getStateFromWarp(VectorXd &state_vec, 
	const Matrix3d& sim_mat){
	VALIDATE_SSM_STATE(state_vec);
	VALIDATE_SIM_WARP(sim_mat);

	state_vec(0) = sim_mat(0, 2);
	state_vec(1) = sim_mat(1, 2);
	state_vec(2) = sim_mat(0, 0) - 1;
	state_vec(3) = sim_mat(1, 0);
}

void Similitude::cmptInitPixJacobian(MatrixXd &jacobian_prod,
	const PixGradT &pix_jacobian){
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);

	for (int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = Ix;
		jacobian_prod(i, 1) = Iy;
		jacobian_prod(i, 2) = Ix*x + Iy*y;
		jacobian_prod(i, 3) = Iy*x - Ix*y;
	}
}

void Similitude::getInitPixGrad(Matrix2Xd &ssm_grad, int pix_id) {
	double x = init_pts(0, pix_id);
	double y = init_pts(1, pix_id);
	ssm_grad <<
		1, 0, x, -y,
		0, 1, y, x;
}

void Similitude::cmptInitPixHessian(MatrixXd &pix_hess_ssm, const PixHessT &pix_hess_coord,
	const PixGradT &pix_grad){
	VALIDATE_SSM_HESSIAN(pix_hess_ssm, pix_hess_coord, pix_grad);

	Matrix24d ssm_grad;
	for(int pt_id = 0; pt_id < n_pts; pt_id++){
		double x = init_pts(0, pt_id);
		double y = init_pts(1, pt_id);
		ssm_grad <<
			1, 0, x, -y,
			0, 1, y, x;
		Map<Matrix4d> curr_pix_hess_ssm((double*)pix_hess_ssm.col(pt_id).data());
		curr_pix_hess_ssm = ssm_grad.transpose()*Map<Matrix2d>((double*)pix_hess_coord.col(pt_id).data())*ssm_grad;
	}
}

void Similitude::cmptApproxPixJacobian(MatrixXd &jacobian_prod, const PixGradT &pix_jacobian) {
	VALIDATE_SSM_JACOBIAN(jacobian_prod, pix_jacobian);
	double a_plus_1 = curr_state(2) + 1, b = curr_state(3);
	double inv_det = 1.0 / (a_plus_1*a_plus_1 + b*b);

	for(int i = 0; i < n_pts; i++){
		double x = init_pts(0, i);
		double y = init_pts(1, i);
		double Ix = pix_jacobian(i, 0);
		double Iy = pix_jacobian(i, 1);

		jacobian_prod(i, 0) = (Ix*a_plus_1 - Iy*b) * inv_det;
		jacobian_prod(i, 1) = (Ix*b + Iy*a_plus_1) * inv_det;
		jacobian_prod(i, 2) = (Ix*(x*a_plus_1 + y*b) + Iy*(y*a_plus_1 - x*b)) * inv_det;
		jacobian_prod(i, 3) = (Ix*(-y*a_plus_1 + x*b) + Iy*(x*a_plus_1 + y*b)) * inv_det;
	}
}

void Similitude::estimateWarpFromCorners(VectorXd &state_update, const Matrix24d &in_corners,
	const Matrix24d &out_corners){
	VALIDATE_SSM_STATE(state_update);

	Matrix3d warp_update_mat = utils::computeSimilitudeDLT(in_corners, out_corners);
	getStateFromWarp(state_update, warp_update_mat);
}

void Similitude::updateGradPts(double grad_eps){
	Vector2d diff_vec_x_warped = curr_warp.topRows<2>().col(0) * grad_eps;
	Vector2d diff_vec_y_warped = curr_warp.topRows<2>().col(1) * grad_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){
		grad_pts(0, pix_id) = curr_pts(0, pix_id) + diff_vec_x_warped(0);
		grad_pts(1, pix_id) = curr_pts(1, pix_id) + diff_vec_x_warped(1);

		grad_pts(2, pix_id) = curr_pts(0, pix_id) - diff_vec_x_warped(0);
		grad_pts(3, pix_id) = curr_pts(1, pix_id) - diff_vec_x_warped(1);

		grad_pts(4, pix_id) = curr_pts(0, pix_id) + diff_vec_y_warped(0);
		grad_pts(5, pix_id) = curr_pts(1, pix_id) + diff_vec_y_warped(1);

		grad_pts(6, pix_id) = curr_pts(0, pix_id) - diff_vec_y_warped(0);
		grad_pts(7, pix_id) = curr_pts(1, pix_id) - diff_vec_y_warped(1);
	}
}


void Similitude::updateHessPts(double hess_eps){
	double hess_eps2 = 2 * hess_eps;

	Vector2d diff_vec_xx_warped = curr_warp.topRows<2>().col(0) * hess_eps2;
	Vector2d diff_vec_yy_warped = curr_warp.topRows<2>().col(1) * hess_eps2;
	Vector2d diff_vec_xy_warped = (curr_warp.topRows<2>().col(0) + curr_warp.topRows<2>().col(1)) * hess_eps;
	Vector2d diff_vec_yx_warped = (curr_warp.topRows<2>().col(0) - curr_warp.topRows<2>().col(1)) * hess_eps;

	for(int pix_id = 0; pix_id < n_pts; pix_id++){

		hess_pts(0, pix_id) = curr_pts(0, pix_id) + diff_vec_xx_warped(0);
		hess_pts(1, pix_id) = curr_pts(1, pix_id) + diff_vec_xx_warped(1);

		hess_pts(2, pix_id) = curr_pts(0, pix_id) - diff_vec_xx_warped(0);
		hess_pts(3, pix_id) = curr_pts(1, pix_id) - diff_vec_xx_warped(1);

		hess_pts(4, pix_id) = curr_pts(0, pix_id) + diff_vec_yy_warped(0);
		hess_pts(5, pix_id) = curr_pts(1, pix_id) + diff_vec_yy_warped(1);

		hess_pts(6, pix_id) = curr_pts(0, pix_id) - diff_vec_yy_warped(0);
		hess_pts(7, pix_id) = curr_pts(1, pix_id) - diff_vec_yy_warped(1);

		hess_pts(8, pix_id) = curr_pts(0, pix_id) + diff_vec_xy_warped(0);
		hess_pts(9, pix_id) = curr_pts(1, pix_id) + diff_vec_xy_warped(1);

		hess_pts(10, pix_id) = curr_pts(0, pix_id) - diff_vec_xy_warped(0);
		hess_pts(11, pix_id) = curr_pts(1, pix_id) - diff_vec_xy_warped(1);

		hess_pts(12, pix_id) = curr_pts(0, pix_id) + diff_vec_yx_warped(0);
		hess_pts(13, pix_id) = curr_pts(1, pix_id) + diff_vec_yx_warped(1);

		hess_pts(14, pix_id) = curr_pts(0, pix_id) - diff_vec_yx_warped(0);
		hess_pts(15, pix_id) = curr_pts(1, pix_id) - diff_vec_yx_warped(1);
	}
}
void Similitude::applyWarpToCorners(Matrix24d &warped_corners, const Matrix24d &orig_corners,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	for(int i = 0; i < 4; i++){
		warped_corners(0, i) = warp_mat(0, 0)*orig_corners(0, i) + warp_mat(0, 1)*orig_corners(1, i) +
			warp_mat(0, 2);
		warped_corners(1, i) = warp_mat(1, 0)*orig_corners(0, i) + warp_mat(1, 1)*orig_corners(1, i) +
			warp_mat(1, 2);
	}
}

void Similitude::applyWarpToPts(Matrix2Xd &warped_pts, const Matrix2Xd &orig_pts,
	const VectorXd &ssm_state){
	getWarpFromState(warp_mat, ssm_state);
	int n_pts = orig_pts.cols();
	for(int i = 0; i < n_pts; i++){
		warped_pts(0, i) = warp_mat(0, 0)*orig_pts(0, i) + warp_mat(0, 1)*orig_pts(1, i) +
			warp_mat(0, 2);
		warped_pts(1, i) = warp_mat(1, 0)*orig_pts(0, i) + warp_mat(1, 1)*orig_pts(1, i) +
			warp_mat(1, 2);
	}
}

_MTF_END_NAMESPACE

