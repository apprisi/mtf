#ifndef MTF_PRE_PROC_H
#define MTF_PRE_PROC_H

// some basic functions for preprocessing the raw input image 
// before using it for tracking

#include "opencv2/imgproc/imgproc.hpp"
#include "mtf/Config/parameters.h"

const vector<int> supported_output_types = { CV_32FC3, CV_32FC1, CV_8UC3, CV_8UC1 };

struct PreProc{
private:
	cv::Mat frame_rgb, frame_gs, frame_rgb_uchar;
	int output_type;
	int frame_id;
	bool rgb_input;
public:
	PreProc *next;
	PreProc(int _output_type = CV_32FC1) :
		output_type(_output_type),
		frame_id(-1),
		rgb_input(true), 
		next(nullptr){}
	virtual ~PreProc(){
		frame_rgb.release();
		frame_gs.release();
		frame_rgb_uchar.release();
	}
	virtual void initialize(const cv::Mat &frame_raw, int _frame_id = -1){
		if(_frame_id > 0 && frame_id == _frame_id){ return; }// this frame has already been processed
		frame_id = _frame_id;
		switch(frame_raw.type()){
		case CV_8UC3:
			printf("PreProc::Input type: CV_8UC3 ");
			break;
		case CV_8UC1:
			printf("PreProc::Input type: CV_8UC1 ");
			rgb_input = false;
			break;
		default:
			throw std::invalid_argument(
				cv::format("Invalid input image type provided: %d", frame_raw.type()));
		}
		switch(output_type){
		case CV_32FC1:
			printf("Output type: CV_32FC1\n");
			if(rgb_input){
				frame_rgb.create(frame_raw.rows, frame_raw.cols, CV_32FC3);
			}
			frame_gs.create(frame_raw.rows, frame_raw.cols, CV_32FC1);
			break;
		case CV_8UC1:
			printf("Output type: CV_8UC1\n");
			frame_gs.create(frame_raw.rows, frame_raw.cols, CV_8UC1);
			break;
		case CV_32FC3:
			printf("Output type: CV_32FC3\n");
			if(!rgb_input){
				frame_rgb_uchar.create(frame_raw.rows, frame_raw.cols, CV_8UC3);
			}
			frame_rgb.create(frame_raw.rows, frame_raw.cols, CV_32FC3);
			break;
		case CV_8UC3:
			printf("Output type: CV_8UC3\n");
			frame_rgb.create(frame_raw.rows, frame_raw.cols, CV_8UC3);
			break;
		default:
			throw std::invalid_argument("Invalid output image type provided");
		}
		processFrame(frame_raw);
		if(next){ next->initialize(frame_raw, _frame_id); }
	}
	virtual void update(const cv::Mat &frame_raw, int _frame_id = -1){
		if(_frame_id > 0 && frame_id == _frame_id){ return; }// this frame has already been processed
		frame_id = _frame_id;
		processFrame(frame_raw);
		if(next){ next->update(frame_raw, _frame_id); }
	}
	virtual const cv::Mat& getFrame(){
		switch(output_type){
		case CV_32FC1:
			return frame_gs;
		case CV_8UC1:
			return frame_gs;
		case CV_32FC3:
			return frame_rgb;
		case CV_8UC3:
			return frame_rgb;
		default:
			throw std::invalid_argument("Invalid output image type provided");
		}
	}
	virtual void showFrame(std::string window_name){
		cv::Mat  disp_img;
		switch(output_type){
		case CV_32FC1:
			disp_img.create(frame_gs.rows, frame_gs.cols, CV_8UC1);
			frame_gs.convertTo(disp_img, disp_img.type());
			imshow(window_name, disp_img);
			break;
		case CV_8UC1:
			imshow(window_name, frame_gs);
			break;
		case CV_32FC3:
			disp_img.create(frame_gs.rows, frame_gs.cols, CV_8UC3);
			frame_rgb.convertTo(disp_img, disp_img.type());
			imshow(window_name, disp_img);
			break;
		case CV_8UC3:
			imshow(window_name, frame_rgb);
			break;
		default:
			throw std::invalid_argument("Invalid output image type provided");
		}
	}
	virtual int outputType() const{ return output_type; }
	virtual void setFrameID(int _frame_id){ frame_id = _frame_id; }
	virtual int getFrameID() const{ return frame_id; }

protected:

	virtual void processFrame(const cv::Mat &frame_raw){
		switch(output_type){
		case CV_32FC1:
			if(rgb_input){
				frame_raw.convertTo(frame_rgb, frame_rgb.type());
				cv::cvtColor(frame_rgb, frame_gs, CV_BGR2GRAY);
			} else{
				frame_raw.convertTo(frame_gs, frame_gs.type());
			}
			apply(frame_gs);
			break;
		case CV_8UC1:
			if(rgb_input){
				cv::cvtColor(frame_raw, frame_gs, CV_BGR2GRAY);
			} else{
				frame_raw.copyTo(frame_gs);
			}
			apply(frame_gs);
			break;
		case CV_32FC3:
			if(rgb_input){
				frame_raw.convertTo(frame_rgb, frame_rgb.type());
			} else{
				cv::cvtColor(frame_raw, frame_rgb_uchar, CV_GRAY2BGR);
				frame_rgb_uchar.convertTo(frame_rgb, frame_rgb.type());
			}
			apply(frame_rgb);
			break;
		case CV_8UC3:
			if(rgb_input){
				frame_raw.copyTo(frame_rgb);
			} else{
				cv::cvtColor(frame_raw, frame_rgb, CV_GRAY2BGR);
			}
			apply(frame_rgb);
			break;
		default:
			throw std::invalid_argument(
				cv::format("Invalid output image type provided: %d", output_type));
		}
	}	
	virtual void apply(cv::Mat &img_gs) const = 0;
};

struct GaussianSmoothing : public PreProc{
private:
	cv::Size kernel_size;
	double sigma_x;
	double sigma_y;
public:
	GaussianSmoothing(
		int _output_type = CV_32FC1,
		int _kernel_size = 5,
		double _sigma_x = 3.0,
		double _sigma_y = 0) :
		PreProc(_output_type),
		kernel_size(_kernel_size, _kernel_size),
		sigma_x(_sigma_x),
		sigma_y(_sigma_y){
		printf("Using Gaussian Smoothing with ");
		printf("kernel_size: %d x %d and sigma: %f x %f\n",
			kernel_size.width, kernel_size.height, sigma_x, sigma_y);
	}
	void apply(cv::Mat &img_gs) const override{
		cv::GaussianBlur(img_gs, img_gs, kernel_size, sigma_x, sigma_y);
	}
};
struct MedianFiltering : public PreProc{
private:
	int kernel_size;
public:
	MedianFiltering(
		int _output_type = CV_32FC1,
		int _kernel_size = 5) :
		PreProc(_output_type), kernel_size(_kernel_size){
		printf("Using Median Filtering with ");
		printf("kernel_size: %d\n", kernel_size);
	}
	void apply(cv::Mat &img_gs) const override{
		cv::medianBlur(img_gs, img_gs, kernel_size);
	}
};
struct NormalizedBoxFltering : public PreProc{
private:
	cv::Size kernel_size;
public:
	NormalizedBoxFltering(
		int _output_type = CV_32FC1,
		int _kernel_size = 5) :
		PreProc(_output_type), kernel_size(_kernel_size, _kernel_size){
		printf("Using Normalized Box Fltering with ");
		printf("kernel_size: %d x %d\n", kernel_size.width, kernel_size.height);
	}
	void apply(cv::Mat &img_gs) const override{
		cv::blur(img_gs, img_gs, kernel_size);
	}
};
struct BilateralFiltering : public PreProc{
private:
	int diameter;
	double sigma_col;
	double sigma_space;
public:
	BilateralFiltering(
		int _output_type = CV_32FC1,
		int _diameter = 5,
		double _sigma_col = 15,
		double _sigma_space = 15) :
		PreProc(_output_type), diameter(_diameter),
		sigma_col(_sigma_col), sigma_space(_sigma_space){
		printf("Using Bilateral Filtering with ");
		printf("diameter: %d, sigma_col: %f and sigma_space: %f\n",
			diameter, sigma_col, sigma_space);
	}
	void apply(cv::Mat &img_gs) const override{
		// OpenCV bilateral filterig does not work in place so a copy has to be made
		cv::Mat orig_img = img_gs.clone();
		//img_gs.copyTo(orig_img);
		cv::bilateralFilter(orig_img, img_gs, diameter, sigma_col, sigma_space);
	}
};
struct NoProcessing : public PreProc{
	NoProcessing(int _output_type = CV_32FC1) : PreProc(_output_type){}
	void apply(cv::Mat &img_gs) const override{}
};

#endif